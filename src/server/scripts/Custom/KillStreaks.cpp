struct KillStreak
{
    uint32 kills;
    uint32 lastKilledGuid; // Prevent gaining kills from previously killed player
};

std::unordered_map<uint32, KillStreak> KillStreakContainer;
std::mutex killstreak_mutex;

class player_killstreak : public PlayerScript
{
public:
    player_killstreak() : PlayerScript("player_killstreak") { }

    void OnPVPKill(Player* killer, Player* killed) override
    {
        uint32 killerGuid = killer->GetGUID().GetCounter();
        uint32 killedGuid = killed->GetGUID().GetCounter();
        const char* killedName = killed->GetName().c_str();
        bool wasMyLastVictim = false;
        uint32 killedKills = 1;

        {
            std::lock_guard<std::mutex> guard(killstreak_mutex);
            wasMyLastVictim = (KillStreakContainer[killerGuid].lastKilledGuid == killedGuid);
            killedKills = KillStreakContainer[killedGuid].kills;

            KillStreakContainer[killerGuid].lastKilledGuid = killedGuid; // Log our previous kill (prevent farming)
            KillStreakContainer[killedGuid].kills = 1; // Reset victim kills
            KillStreakContainer[killedGuid].lastKilledGuid = 10; // Reset victim's last victim

            if (!wasMyLastVictim)
            {
                KillStreakContainer[killerGuid].kills++; // Increment kills

                // Do stuff on streaks below
                switch (KillStreakContainer[killerGuid].kills)
                {
                    case 5: // 5 kills
					    break;
                    case 10: // 10 kills
                        break;
                }
            }
        }

        if (wasMyLastVictim)
        {
            ChatHandler(killer->GetSession()).PSendSysMessage("You killed |cffcc3333%s|r already. Kill another player to earn something from him/her again.", killedName);
            ChatHandler(killed->GetSession()).SendSysMessage("The person who killed you did not benefit from your death. They killed you before. Now laugh at them!");
            return;
        }

        std::ostringstream ss;
        ss << killer->GetName()
            << " has ruined " << killed->GetName() << "'s"
            << " KillStreak of " << killedKills;

        sWorld->SendGlobalText(ss.str().c_str(), NULL);
    }
};

void SetupKillStreak()
{
    new player_killstreak;
}
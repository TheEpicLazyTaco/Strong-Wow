#include "ScriptPCH.h"

class player_reward : CreatureScript
{
public:
	player_reward() : CreatureScript("player_reward") {}

	//Time Conversion Variables & Methods
	uint32 days, hours, minutes;

	//Returns total ammount of minutes.
	uint32 GetMinutes(uint32 seconds)
	{
		return seconds / 60;
	}

	//Returns total ammount of hours.
	uint32 GetHours(uint32 minutes)
	{
		return minutes / 60;
	}

	//Returns total ammount of days.
	uint32 GetDays(uint32 hours)
	{
		return hours / 24;
	}

	//Returns a string your played time like /played does.
	std::string GetPlayedTime(uint32 seconds)
	{
		std::string _str;

		minutes = seconds / 60;
		hours = minutes / 60;
		days = hours / 24;

		_str = "Total Played Time: " + std::to_string(days % 24) + " days, " + std::to_string(hours % 60) + " hours, " + std::to_string(minutes % 60) + " minutes, " + std::to_string(seconds % 60) + " seconds";
		return _str;
	}

	// Custom Table Functions
	uint32 GetCustomEventProgress(uint32 guid, uint32 id)
	{
		QueryResult res = CharacterDatabase.PQuery("SELECT completed FROM custom_event_progress WHERE guid = '%u' AND id = '%u'", guid, id);
		if (res != NULL)
		{
			Field * fields = res->Fetch();
			uint32 completed = fields[0].GetUInt32();
			return completed;
		}
		return 0;
	}

	void SetCustomEventProgress(uint32 guid, uint32 id, uint32 completed)
	{
		QueryResult res = CharacterDatabase.PQuery("SELECT completed FROM custom_event_progress WHERE guid = '%u' AND id = '%u'", guid, id);
		if (res == NULL)
		{
			CharacterDatabase.PQuery("INSERT INTO `custom_event_progress` SET `guid`='%u', `id`='%u', `completed`='%u'", guid, id, completed);
		}
		else
			CharacterDatabase.PQuery("UPDATE `custom_event_progress` SET `guid`='%u', `id`='%u', `completed`='%u' WHERE guid = '%u' and id = '%u'", guid, id, completed, guid, id);
	}

	uint32 lowguid;


	// Gossip Methods
	virtual bool OnGossipHello(Player* player, Creature* creature)
	{
		lowguid = player->GetGUID().GetCounter();
		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_chargepositive:18:18:-21:0|t What is my played time?", GOSSIP_SENDER_MAIN, 101);
		if (GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) >= 7 && !GetCustomEventProgress(lowguid, 1) == 1)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t|cff2d8b27 7 Day Reward Click To Claim!", GOSSIP_SENDER_MAIN, 102);
		if (GetDays(GetHours(GetHours(GetMinutes(player->GetTotalPlayedTime())))) >= 15 && !GetCustomEventProgress(lowguid, 2) == 1)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t|cff0d120d 15 Day Reward Click To Claim!", GOSSIP_SENDER_MAIN, 103);
		if (GetDays(GetHours(GetHours(GetMinutes(player->GetTotalPlayedTime())))) >= 25 && !GetCustomEventProgress(lowguid, 3) == 1)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t|cffff3929 25 Day Reward Click To Claim!", GOSSIP_SENDER_MAIN, 104);
		if (GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) < 7)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t Your next reward is at 7 day playtime.", GOSSIP_SENDER_MAIN, 999);
		if (GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) < 15 && GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) > 7)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t Your next reward is at 15 day playtime.", GOSSIP_SENDER_MAIN, 999);
		if (GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) < 25 && GetDays(GetHours(GetMinutes(player->GetTotalPlayedTime()))) > 15)
			player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_holy_fanaticism:18:18:-21:0|t Your next reward is at 25 day playtime.", GOSSIP_SENDER_MAIN, 999);
		player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_chargenegative:18:18:-21:0|t Exit", GOSSIP_SENDER_MAIN, 999);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	virtual bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		if (!sender == GOSSIP_SENDER_MAIN)
			return false;

		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{
		case 101:
			ChatHandler(player->GetSession()).PSendSysMessage("%s", GetPlayedTime(player->GetTotalPlayedTime()));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 102:
			ChatHandler(player->GetSession()).PSendSysMessage("Claimed Played Time Event Reward 1!");
			SetCustomEventProgress(lowguid, 1, 1);
			player->AddItem(309991, 1);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 103:
			ChatHandler(player->GetSession()).PSendSysMessage("Claimed Played Time Event Reward 2!");
			SetCustomEventProgress(lowguid, 2, 1);
			player->AddItem(432953, 1);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 104:
			ChatHandler(player->GetSession()).PSendSysMessage("Claimed Played Time Event Reward 3!");
			SetCustomEventProgress(lowguid, 3, 1);
			player->AddItem(250011, 1);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 999:
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}
		return true;
	}
};

void AddSC_player_reward()
{
	new player_reward();
}
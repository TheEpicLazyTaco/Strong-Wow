//Made by nikolaise^ 
// You need rochet2's item gossip script in order for this script to work.
#include "Define.h"
#include "GossipDef.h"
#include "Item.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "GameEventMgr.h"
#include "Player.h"
#include "WorldSession.h"

class PortableTeleporter_ItemGossip : public ItemScript
{
public:
	PortableTeleporter_ItemGossip() : ItemScript("PortableTeleporter_ItemGossip") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& targets) override 
    {
		if ((player->GetZoneId() == 2597 || player->InBattleground() || player->GetZoneId() == 33 || player->GetZoneId() == 357 || player->GetZoneId() == 268) || player->IsInCombat())
		{
			player->GetSession()->SendAreaTriggerMessage("Your either combat or a battleground and can't use me!");
		}
		else
		{
			player->SummonCreature(999991, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_TIMED_OR_DEAD_DESPAWN, 100000);

			if (Creature* creature = player->FindNearestCreature(999991, 25.0f))
			{
				creature->GetMotionMaster()->MoveFollow(player, PET_FOLLOW_DIST, creature->GetFollowAngle(), MOTION_SLOT_ACTIVE);
			}
		}
		return true;
	}
};


class PortableTeleporter : public  CreatureScript
{
public:
	PortableTeleporter() : CreatureScript("PortableTeleporter")
	{
	}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface/ICONS/INV_Misc_Map08:35:35:-23:0|t-> Teleport me", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface/ICONS/Spell_Holy_CircleOfRenewal:35:35:-23:0|t-> Heal me", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface/ICONS/Ability_Repair:35:35-:-23:0|t-> Repair My Items", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface/ICONS/inv_misc_truegold:35:35:-23:0|t-> Bank's ", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface/ICONS/Achievement_Boss_CThun:35:35:-23:0|t-> Morphs", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_bg_returnxflags_def_wsg:35:35:-23:0|t<- Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature * creature, uint32 Sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		if (Sender == GOSSIP_SENDER_MAIN)
			switch (action)
		{
			//Nevermind
			case GOSSIP_ACTION_INFO_DEF + 7:
				player->CLOSE_GOSSIP_MENU();
				break;

			//morphs
			case GOSSIP_ACTION_INFO_DEF + 6:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Shadow_DemonicCircleTeleport:35:35:-23:0|t-> Race Morphs", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 400);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Shadow_DemonicCircleTeleport:35:35:-23:0|t-> Custom Morphs", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 401);
				player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
				break;
			case GOSSIP_ACTION_INFO_DEF + 400:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Human Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 43);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Human Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 44);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Dwarf Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 45);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Dwarf Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 46);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Night Elf Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 47);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Night Elf Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 48);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Gnome Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 49);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Gnome Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 50);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Dreanai Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 51);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT,  "Dreanai Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 52);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1,"Next Page", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 53);
				break;

			case GOSSIP_ACTION_INFO_DEF + 53:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Orc Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 55);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Orc Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 56);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Undead Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 57);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Undead Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 58);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tauren Elf Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 59);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tauren Elf Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 60);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Troll Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 61);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Troll Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 62);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Blood Elf Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 63);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Blood Elf Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 64);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 400);
				break;
			case GOSSIP_ACTION_INFO_DEF + 401:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Arthas", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 402);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Penguin", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 403);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Demon Hunter", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 404);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Female Death Knight", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 405);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tuskarr", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 406);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Warlock Green", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 407);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Pirate Female", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 408);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Pirate Male", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 409);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Undead Death Knight", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 410);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Ghost Tuskarr", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 411);

				// Alliance Morphs

			case GOSSIP_ACTION_INFO_DEF + 43:
				player->SetDisplayId(19724);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;

			case GOSSIP_ACTION_INFO_DEF + 44:
				player->SetDisplayId(19723);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 45:
				player->SetDisplayId(37918);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;

			case GOSSIP_ACTION_INFO_DEF + 46:
				player->SetDisplayId(20317);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
						
			case GOSSIP_ACTION_INFO_DEF + 47:
				player->SetDisplayId(37919);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
							
			case GOSSIP_ACTION_INFO_DEF + 48:
				player->SetDisplayId(20318);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
							
			case GOSSIP_ACTION_INFO_DEF + 49:
				player->SetDisplayId(20320);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;

			
			case GOSSIP_ACTION_INFO_DEF + 50:
				player->SetDisplayId(20580);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
							
			case GOSSIP_ACTION_INFO_DEF + 51:
				player->SetDisplayId(20323);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
		
			case GOSSIP_ACTION_INFO_DEF + 52:
				player->SetDisplayId(37916);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
				//Horde Morphs
			case GOSSIP_ACTION_INFO_DEF + 55:
				player->SetDisplayId(20316);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 56:
				player->SetDisplayId(37920);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 57:
				player->SetDisplayId(37924);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 58:
				player->SetDisplayId(37923);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 59:
				player->SetDisplayId(37921);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 60:
				player->SetDisplayId(20319);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 61:
				player->SetDisplayId(37922);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 62:
				player->SetDisplayId(20321);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 63:
				player->SetDisplayId(20578);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 64:
				player->SetDisplayId(20579);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
				//Custom Morphs
			case GOSSIP_ACTION_INFO_DEF + 402:
				player->SetDisplayId(24949);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 403:
				player->SetDisplayId(24593);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 404:
				player->SetDisplayId(37924);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 405:
				player->SetDisplayId(24935);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 406:
				player->SetDisplayId(24391);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 407:
				player->SetDisplayId(25006);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 408:
				player->SetDisplayId(25048);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 409:
				player->SetDisplayId(25037);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 410:
				player->SetDisplayId(25580);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;
			case GOSSIP_ACTION_INFO_DEF + 411:
				player->SetDisplayId(25552);
				creature->Whisper("You've been Morphed!", LANG_UNIVERSAL, player);
				break;

				//Set Maxhealth
			case GOSSIP_ACTION_INFO_DEF + 2: 
				if (player->IsInCombat())
				{
					creature->Whisper("You're in combat and will not be healed!", LANG_UNIVERSAL, player);
					player->CLOSE_GOSSIP_MENU();
					break;
				}
				else
				{
					player->CLOSE_GOSSIP_MENU();
					player->GetMaxHealth();
					creature->Whisper("You have been Healed!", LANG_UNIVERSAL, player);
					break;
				}

				//Bank
			case GOSSIP_ACTION_INFO_DEF + 4:
				player->CLOSE_GOSSIP_MENU();
				player->GetSession()->SendShowBank(player->GetGUID());
				break;
				// Repair items
			case GOSSIP_ACTION_INFO_DEF + 3:
				player->DurabilityRepairAll(false, 0, false);
				player->GetSession()->SendNotification("Your items have been repaired!");
				player->CLOSE_GOSSIP_MENU();
				break;

			//Teleport me
			case GOSSIP_ACTION_INFO_DEF + 1:
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Shadow_DemonicCircleTeleport:35:35:-23:0|t-> Main cities", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 19);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_Dungeon_UlduarRaid_Misc_01:35:35:-23:0|t-> Raids", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 20);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_Dungeon_CoTStratholme_10man:35:35:-23:0|t-> Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 300);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_bg_returnxflags_def_wsg:35:35:-23:0|t<- Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
				player->PlayerTalkClass->SendGossipMenu(2, creature->GetGUID());
				return true;

				//Main Cities.
			case GOSSIP_ACTION_INFO_DEF + 19:

				if (player->GetTeam() == ALLIANCE)
				{
					player->PlayerTalkClass->ClearMenus();
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalStormWind:35:35:-23:0|t-> Stormwind", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 21);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalIronForge:35:35:-23:0|t-> Ironforge", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 22);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalDarnassus:35:35:-23:0|t-> Darnassus", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 23);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalExodar:35:35:-23:0|t-> Exodar", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 24);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalTheramore:35:35:-23:0|t-> Mall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 26);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_bg_returnxflags_def_wsg:35:35:-23:0|t<- back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
					player->PlayerTalkClass->SendGossipMenu(3, creature->GetGUID());
					break;
				}
				else
				{
					player->PlayerTalkClass->ClearMenus();
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_TeleportOrgrimmar:35:35:-23:0|t -> Orgrimmar", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_TeleportUnderCity.:35:35:-23:0|t -> Undercity", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalSilvermoon:35:35:-23:0|t -> Silvermoon City", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 13);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalThunderBluff:35:35:-23:0|t-> Thunder Bluff", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Spell_Arcane_PortalTheramore:35:35:-23:0|t -> Mall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 26);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface/ICONS/Achievement_bg_returnxflags_def_wsg:35:35:-23:0|t <- back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
					player->PlayerTalkClass->SendGossipMenu(4, creature->GetGUID());
					break;
				}


			case GOSSIP_ACTION_INFO_DEF + 300: //Dungeons Menu
				player->ADD_GOSSIP_ITEM(2, "Classic Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 301);
				player->ADD_GOSSIP_ITEM(2, "BC Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 302);
				player->ADD_GOSSIP_ITEM(2, "Wrath Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 303);
				player->ADD_GOSSIP_ITEM(2, "Raids", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 399);
				player->ADD_GOSSIP_ITEM(1, "<- Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
				player->PlayerTalkClass->SendGossipMenu(7, creature->GetGUID());
				break;

				// classic dungeons
			case GOSSIP_ACTION_INFO_DEF + 301: 
				player->ADD_GOSSIP_ITEM(2, "Gnomeregan", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 304);
				player->ADD_GOSSIP_ITEM(2, "The Deadmines", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 305);
				player->ADD_GOSSIP_ITEM(2, "The Stockade", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 306);
				player->ADD_GOSSIP_ITEM(2, "Ragefire Chasm", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 307);
				player->ADD_GOSSIP_ITEM(2, "Razorfen Downs", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 308);
				player->ADD_GOSSIP_ITEM(2, "Razorfen Kraul", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 309);
				player->ADD_GOSSIP_ITEM(2, "Scarlet Monastery", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 310);
				player->ADD_GOSSIP_ITEM(2, "Shadowfang Keep", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 311);
				player->ADD_GOSSIP_ITEM(2, "Wailing Caverns", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 312);
				player->ADD_GOSSIP_ITEM(2, "Blackfathom Deeps", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 313);
				player->ADD_GOSSIP_ITEM(2, "Blackrock Depths", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 314);
				player->ADD_GOSSIP_ITEM(2, "Blackrock Spire", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 315);
				player->ADD_GOSSIP_ITEM(2, "Dire Maul", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 316);
				player->ADD_GOSSIP_ITEM(2, "Maraudon", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 317);
				player->ADD_GOSSIP_ITEM(2, "Scholomance", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 318);
				player->ADD_GOSSIP_ITEM(2, "Stratholme", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 319);
				player->ADD_GOSSIP_ITEM(2, "Sunken Temple", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 320);
				player->ADD_GOSSIP_ITEM(2, "Uldaman", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 321);
				player->ADD_GOSSIP_ITEM(2, "Zul'Farrak", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 322);
				player->ADD_GOSSIP_ITEM(1, "<- Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 300);
				player->PlayerTalkClass->SendGossipMenu(8, creature->GetGUID());
				break;

				// BC dungeons
			case GOSSIP_ACTION_INFO_DEF + 302: 
				player->ADD_GOSSIP_ITEM(2, "Tempest Keep", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 323);
				player->ADD_GOSSIP_ITEM(2, "Magisters' Terrace", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 324);
				player->ADD_GOSSIP_ITEM(2, "Hellfire Citadel", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 325);
				player->ADD_GOSSIP_ITEM(2, "Coilfang Reservoir", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 326);
				player->ADD_GOSSIP_ITEM(2, "Caverns of Time", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 327);
				player->ADD_GOSSIP_ITEM(2, "Auchindoun", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 328);
				player->ADD_GOSSIP_ITEM(1, "<- Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 300);
				player->PlayerTalkClass->SendGossipMenu(9, creature->GetGUID());
				break;


				// wrath dungeons
			case GOSSIP_ACTION_INFO_DEF + 303: 
				player->ADD_GOSSIP_ITEM(2, "Utgarde Pinnacle", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 329);
				player->ADD_GOSSIP_ITEM(2, "Utgarde Keep", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 330);
				player->ADD_GOSSIP_ITEM(2, "Halls of Stone", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 331);
				player->ADD_GOSSIP_ITEM(2, "Halls of Lightning", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 332);
				player->ADD_GOSSIP_ITEM(2, "The Violet Hold", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 333);
				player->ADD_GOSSIP_ITEM(2, "The Nexus Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 334);
				player->ADD_GOSSIP_ITEM(2, "Icecrown Citadel Dungeons", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 335);
				player->ADD_GOSSIP_ITEM(2, "Gundrak", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 336);
				player->ADD_GOSSIP_ITEM(2, "Drak'Tharon Keep", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 337);
				player->ADD_GOSSIP_ITEM(2, "Trial of the Champion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 338);
				player->ADD_GOSSIP_ITEM(2, "The Culling of Stratholme", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 339);
				player->ADD_GOSSIP_ITEM(2, "Azjol-Nerub", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 340);
				player->ADD_GOSSIP_ITEM(1, "<- Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 300);
				player->PlayerTalkClass->SendGossipMenu(10, creature->GetGUID());
				break;

				// Raids
			case GOSSIP_ACTION_INFO_DEF + 399:
				player->PlayerTalkClass->ClearMenus();
				player->ADD_GOSSIP_ITEM(2, "Zul'Aman", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 69);
				player->ADD_GOSSIP_ITEM(2, "Zul'Gurub", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 70);
				player->ADD_GOSSIP_ITEM(2, "Vault of Archavon", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 71);
				player->ADD_GOSSIP_ITEM(2, "Ulduar", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 72);
				player->ADD_GOSSIP_ITEM(2, "The Obsidian Sanctum", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 73);
				player->ADD_GOSSIP_ITEM(2, "The Eye of Eternity", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 74);
				player->ADD_GOSSIP_ITEM(2, "Temple of Ahn'Qiraj", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 75);
				player->ADD_GOSSIP_ITEM(2, "The Eye", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 76);
				player->ADD_GOSSIP_ITEM(2, "Sunwell Plateau", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 77);
				player->ADD_GOSSIP_ITEM(2, "Ruins of Ahn'Qiraj", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 78);
				player->ADD_GOSSIP_ITEM(2, "Onyxia's Lair", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 79);
				player->ADD_GOSSIP_ITEM(2, "Naxxramas", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 80);
				player->ADD_GOSSIP_ITEM(2, "Molten Core", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 81);
				player->ADD_GOSSIP_ITEM(2, "Karazhan", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 82);
				player->ADD_GOSSIP_ITEM(2, "Icecrown Citadel", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 83);
				player->ADD_GOSSIP_ITEM(2, "Magtheridon's Lair", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 84);
				player->ADD_GOSSIP_ITEM(2, "Gruul's Lair", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 85);
				player->ADD_GOSSIP_ITEM(2, "Trial of the Crusader", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 86);
				player->ADD_GOSSIP_ITEM(2, "Serpentshrine Cavern", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 87);
				player->ADD_GOSSIP_ITEM(2, "Hyjal Summit", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 88);
				player->ADD_GOSSIP_ITEM(2, "Blackwing Lair", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 89);
				player->ADD_GOSSIP_ITEM(2, "Black Temple", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 90);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
				player->PlayerTalkClass->SendGossipMenu(5, creature->GetGUID());
				break;
				//raids

			case GOSSIP_ACTION_INFO_DEF + 69: //Zul'Aman
				player->TeleportTo(530, 6852.09f, -7989.8f, 189.7f, 4.68225f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 70: //Zul'Gurub
				player->TeleportTo(0, -11916.7f, -1215.72f, 92.289f, 4.72454f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 71: //Vault of Archavon
				player->TeleportTo(571, 5454.11f, 2840.79f, 421.278f, 6.27665f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 72: //Ulduar
				player->TeleportTo(571, 8922.35f, -1208.93f, 1025.5f, 6.03428f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 73: //The Obsidian Sanctum
				player->TeleportTo(571, 3513.83f, 269.829f, -114.085f, 3.25168f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 74: //The Eye of Eternity
				player->TeleportTo(530, 3088.49f, 1381.57f, 184.863f, 4.61973f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 75: //Temple of Ahn'Qiraj
				player->TeleportTo(1, -8240.09f, 1991.32f, 129.072f, 0.941603f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 76: //The Eye
				player->TeleportTo(530, 3088.49f, 1381.57f, 184.863f, 4.61973f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 77: //Sunwell Plateau
				player->TeleportTo(530, 12573.2f, -6774.81f, 15.0904f, 3.13788f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 78: //Ruins of Ahn'Qiraj
				player->TeleportTo(1, -8412.61f, 1503.58f, 29.9291f, 2.69932f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 79: //Onyxia's Lair
				player->TeleportTo(1, -4709.97f, -3728.87f, 54.3618f, 3.77106f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 80: //Naxxramas
				player->TeleportTo(571, 3668.72f, -1262.46f, 243.622f, 4.785f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 81: //Molten Core
				player->TeleportTo(230, 1126.2f, -458.611f, -102.265f, 3.46095f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 82: //Karazhan
				player->TeleportTo(0, -11120.2f, -2015.27f, 47.1869f, 1.91823f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 83: //Icecrown Citadel
				player->TeleportTo(571, 5802.92f, 2077.74f, 636.064f, 3.58921f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 84: //Magtheridon's Lair
				player->TeleportTo(530, -317.102f, 3094.48f, -116.43f, 5.20087f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 85: //Gruul's Lair
				player->TeleportTo(530, 3539.19f, 5090.6f, 3.4088f, 5.93244f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 86: //Trial of the Crusader
				player->TeleportTo(571, 8574.85f, 792.33f, 558.518f, 3.16358f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 87: //Serpentshrine Cavern
				player->TeleportTo(530, 828.098f, 6865.51f, -63.7854f, 6.28049f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 88: //Hyjal Summit
				player->TeleportTo(1, -8175.86f, -4175.53f, -166.181f, 0.932953f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 89: //Blackwing Lair
				player->TeleportTo(469, -7665.66f, -1102.2f, 399.679f, 0.625119f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 90: //Black Temple
				player->TeleportTo(530, -3637.56f, 315.554f, 35.5505f, 2.94599f);
				break;

				// Classic Dungeons
			case GOSSIP_ACTION_INFO_DEF + 304: //Gnomeregan
				player->TeleportTo(90, -327.577f, -4.35046f, -152.845f, 6.18894f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 305: //The Deadmines
				player->TeleportTo(0, -11208.1f, 1671.55f, 24.6908f, 1.54879f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 306: //The Stockade
				player->TeleportTo(0, -8799.15f, 832.718f, 97.6348f, 6.04085f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 307: //Ragefire Chasm
				player->TeleportTo(1, 1809.17f, -4407.18f, -18.6752f, 5.23117f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 308: //Razorfen Downs
				player->TeleportTo(1, -4655.88f, -2521.14f, 81.2055f, 4.21036f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 309: //Razorfen Kraul
				player->TeleportTo(1, -4463.32f, -1664.49f, 82.2581f, 0.841007f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 310: //Scarlet Monastery
				player->TeleportTo(0, 2873.03f, -764.237f, 160.332f, 5.10055f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 311: //Shadowfang Keep
				player->TeleportTo(0, -241.881f, 1543.69f, 76.8921f, 1.1814f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 312: //Wailing Caverns
				player->TeleportTo(1, -732.428f, -2220.71f, 17.3289f, 2.68276f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 313: //Blackfathom Deeps
				player->TeleportTo(1, 4247.74f, 745.942f, -24.2824f, 1.07678f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 314: //Blackrock Depths
				player->TeleportTo(0, -7182.15f, -917.016f, 165.49f, 5.06065f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 315: //Blackrock Spire
				player->TeleportTo(0, -7534.79f, -1212.62f, 285.44f, 5.2131f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 316: //Dire Maul
				player->TeleportTo(1, -3829.83f, 1250.34f, 160.229f, 3.12903f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 317: //Maraudon
				player->TeleportTo(1, -1465.22f, 2617.91f, 76.9758f, 3.21067f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 318: //Scholomance
				player->TeleportTo(0, 1265.68f, -2557.52f, 94.1264f, 0.491748f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 319: //Stratholme
				player->TeleportTo(0, 3342.79f, -3379.33f, 144.776f, 6.26449f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 320: //Sunken Temple
				player->TeleportTo(0, -10183.4f, -3993.19f, -109.195f, 6.03063f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 321: //Uldaman
				player->TeleportTo(0, -6070.1f, -2955.81f, 209.778f, 0.071478f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 322: //Zul'Farrak
				player->TeleportTo(1, -6821.2f, -2890.29f, 8.88035f, 6.24783f);
				break;


				//BC dungeons
			case GOSSIP_ACTION_INFO_DEF + 323: //Tempest Keep
				player->TeleportTo(530, 3090.54f, 1406.45f, 189.574f, 4.62382f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 324: //Magisters' Terrace
				player->TeleportTo(530, 12886.2f, -7333.84f, 65.4884f, 4.26101f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 325: //Hellfire Citadel
				player->TeleportTo(530, -360.681f, 3066.36f, -15.1251f, 1.78001f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 326: //Coilfang Reservoir
				player->TeleportTo(530, 748.466f, 6882.93f, -64.1698f, 3.85811f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 327: //Caverns of Time
				player->TeleportTo(1, -8181.85f, -4703.54f, 19.5836f, 4.95664f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 328: //Auchindoun
				player->TeleportTo(530, -3334.63f, 4933.87f, -100.079f, 0.41726f);
				break;

				//Wrath Dungeons
			case GOSSIP_ACTION_INFO_DEF + 329: //Utgarde Pinnacle
				player->TeleportTo(571, 1245.8f, -4856.45f, 216.813f, 3.42403f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 330: //Utgarde Keep
				player->TeleportTo(571, 1205.7f, -4867.65f, 41.2479f, 0.234685f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 331: //Halls of Stone
				player->TeleportTo(571, 8922.12f, -1009.16f, 1038.56f, 1.57044f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 332: //Halls of Lightning
				player->TeleportTo(571, 9183.53f, -1385.17f, 1110.22f, 5.56943f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 333: //The Violet Hold
				player->TeleportTo(571, 5681.5f, 488.528f, 652.45f, 4.01037f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 334: //The Nexus Dungeons
				player->TeleportTo(571, 3771.01f, 6947.5f, 105.88f, 0.472142f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 335: //Icecrown Citadel Dungeons
				player->TeleportTo(571, 5636.55f, 2064.03f, 798.059f, 4.62278f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 336: //Gundrak
				player->TeleportTo(571, 6726.75f, -4637.28f, 450.605f, 3.90537f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 337: //Drak'Tharon Keep
				player->TeleportTo(571, 4777.85f, -2046.5f, 230.01f, 1.63791f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 338: //Trial of the Champion
				player->TeleportTo(571, 8576.16f, 792.096f, 558.234f, 3.14714f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 339: //The Culling of Stratholme
				player->TeleportTo(1, -8756.39f, -4440.68f, -199.489f, 4.66289f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 340: //Azjol-Nerub
				player->TeleportTo(571, 3679.4f, 2163.25f, 35.7227f, 2.28066f);
				break;

				//Alliance cities:
				//Stormwind
			case GOSSIP_ACTION_INFO_DEF + 21:
				player->TeleportTo(0, -8844.01f, 644.99f, 96.2429f, 5.33693f);
				break;
				//Ironforge
			case GOSSIP_ACTION_INFO_DEF + 22:
				player->TeleportTo(0, -4988.27f, -874.123f, 496.984f, 5.43054f);
				break;
				//Darnassus
			case GOSSIP_ACTION_INFO_DEF + 23:
				player->TeleportTo(1, 9949.24f, 2481.71f, 1316.2f, 2.11659f);
				break;
				//Exodar
			case GOSSIP_ACTION_INFO_DEF + 24:
				player->TeleportTo(530, -3980.04f, -11638.001f, -138.987f, 5.77268f);
				break;
				//Mall
			case GOSSIP_ACTION_INFO_DEF + 26:
				player->TeleportTo(530, -249.446762f, 1017.844666f, 54.327511f, 1.601020f);
				player->ResurrectPlayer(100);
				creature->Whisper("You have been teleported to the mall entrance!", LANG_UNIVERSAL, player);
				break;

				//horde cities
			case GOSSIP_ACTION_INFO_DEF + 11: //Orgrimmar
				player->TeleportTo(1, 1486.66f, -4415.39f, 24.1115f, 0.042373f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 12: //Undercity
				player->TeleportTo(0, 1560.21f, 240.068f, -43.1026f, 0.036295f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 13: //Thunder Bluff
				player->TeleportTo(1, -1197.09f, 29.4339f, 176.95f, 4.71385f);
				break;
			case GOSSIP_ACTION_INFO_DEF + 14: //Silvermoon
				player->TeleportTo(530, 9410.71f, -7277.97f, 14.1808f, 6.28265f);
				break;




		}
		return true;
	}

};


void AddSC_PortableTeleporter_ItemGossip() 
{
    new PortableTeleporter_ItemGossip();
	new PortableTeleporter();
}
#include "ScriptPCH.h"

class casino : public CreatureScript
{
	public:
		casino() : CreatureScript("casino"){}

	bool OnGossipHello(Player * pPlayer, Creature * pCreature)
	{
		pPlayer->ADD_GOSSIP_ITEM(0, "How it works", GOSSIP_SENDER_MAIN, 100);
		pPlayer->ADD_GOSSIP_ITEM(0, "Gamble 5 DP", GOSSIP_SENDER_MAIN, 101);
		pPlayer->ADD_GOSSIP_ITEM(0, "No Thanks", GOSSIP_SENDER_MAIN, 999);
		pPlayer->SEND_GOSSIP_MENU(1, pCreature->GetGUID());
		return true;
	}


	bool OnGossipSelect(Player * pPlayer, Creature * Creature, uint32 /*uiSender*/, uint32 uiAction)
	{
		pPlayer->PlayerTalkClass->ClearMenus();

		switch (uiAction)
		{
			case 100:
				ChatHandler(pPlayer->GetSession()).PSendSysMessage("Once you hit the button it will take 5 dp and roll the odds. You CAN NOT refund the gamble.");
				pPlayer->PlayerTalkClass->SendCloseGossip();
				break;

			case 101:
				if (getDonorpoints(pPlayer) >= 5)
				{
					updateDonorPoints(pPlayer, 5, false);
					switch (urand(0, 100))
					{
						case 13:
							updateDonorPoints(pPlayer, 10, true);
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You win 10 DP!");
							break;
						case 22:
							updateDonorPoints(pPlayer, 10, true);
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You win 10 DP!");
							break;
							
						case 43:
							updateDonorPoints(pPlayer, 10, true);
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You win 10 DP!");
							break;
							
						case 75:
							updateDonorPoints(pPlayer, 10, true);
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You win 10 DP!");
							break;
							
						case 92:
							updateDonorPoints(pPlayer, 10, true);
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You win 10 DP!");
							break;
							
						default:
							ChatHandler(pPlayer->GetSession()).PSendSysMessage("You don't win anything!");
							break;
					}
					pPlayer->PlayerTalkClass->SendCloseGossip();
				}
				else
				{
					ChatHandler(pPlayer->GetSession()).PSendSysMessage("You don't have enough donor points!");
					pPlayer->PlayerTalkClass->SendCloseGossip();
				}
				break;
				

			case 999:
				pPlayer->PlayerTalkClass->SendCloseGossip();
				break;
		}
		return true;
	}

	uint32 getDonorpoints(Player* pPlayer)
	{
		QueryResult res = LoginDatabase.PQuery("SELECT dp FROM sitez.account_data WHERE id = '%u'", pPlayer->GetSession()->GetAccountId());
		Field * fields = res->Fetch();
		uint32 points = fields[0].GetUInt32();
		return points;
	}
	
	void updateDonorPoints(Player * pPlayer, int dp, bool add)
	{
		if(add)
			LoginDatabase.PQuery("Update sitez.account_data Set dp = dp + %u WHERE id = '%u'", dp, pPlayer->GetSession()->GetAccountId());
		else
			LoginDatabase.PQuery("Update sitez.account_data Set dp = dp - %u WHERE id = '%u'", dp, pPlayer->GetSession()->GetAccountId());
	}
};

void AddSC_casino()
{
	new casino();
}
#include "ScriptMgr.h"
// .. more includes
 
class my_script_class: CreatureScript
{
public:
    my_script_class() : CreatureScript("my_script") {}
 
    // more code .. 
};
  
void AddSC_my_script()
{
    new my_script_class();
} 
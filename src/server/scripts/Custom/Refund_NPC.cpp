#include "ScriptMgr.h"

class taco_refund_npc : CreatureScript
{
public:
	taco_refund_npc() : CreatureScript("taco_refund_npc") {}

	void GetClassRefundMenu(Player * plr, Creature * cre)
	{
		switch (plr->getClass())
		{
		case CLASS_WARRIOR:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Helm", GOSSIP_SENDER_MAIN, 201);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Shoulders", GOSSIP_SENDER_MAIN, 202);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Chest", GOSSIP_SENDER_MAIN, 203);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Wrist", GOSSIP_SENDER_MAIN, 204);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Hands", GOSSIP_SENDER_MAIN, 205);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Belt", GOSSIP_SENDER_MAIN, 206);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Legs", GOSSIP_SENDER_MAIN, 207);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warrior Boots", GOSSIP_SENDER_MAIN, 208);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_PALADIN:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Helm", GOSSIP_SENDER_MAIN, 211);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Shoulders", GOSSIP_SENDER_MAIN, 212);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Chest", GOSSIP_SENDER_MAIN, 213);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Wrist", GOSSIP_SENDER_MAIN, 214);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Hands", GOSSIP_SENDER_MAIN, 215);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Belt", GOSSIP_SENDER_MAIN, 216);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Legs", GOSSIP_SENDER_MAIN, 217);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Paladin Boots", GOSSIP_SENDER_MAIN, 218);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_SHAMAN:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Helm", GOSSIP_SENDER_MAIN, 221);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Shoulders", GOSSIP_SENDER_MAIN, 222);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Chest", GOSSIP_SENDER_MAIN, 223);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Wrist", GOSSIP_SENDER_MAIN, 224);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Hands", GOSSIP_SENDER_MAIN, 225);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Belt", GOSSIP_SENDER_MAIN, 226);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Legs", GOSSIP_SENDER_MAIN, 227);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Shaman Boots", GOSSIP_SENDER_MAIN, 228);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_WARLOCK:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Helm", GOSSIP_SENDER_MAIN, 231);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Shoulders", GOSSIP_SENDER_MAIN, 232);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Chest", GOSSIP_SENDER_MAIN, 233);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Wrist", GOSSIP_SENDER_MAIN, 234);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Hands", GOSSIP_SENDER_MAIN, 235);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Belt", GOSSIP_SENDER_MAIN, 236);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Legs", GOSSIP_SENDER_MAIN, 237);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Warlock Boots", GOSSIP_SENDER_MAIN, 238);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_DEATH_KNIGHT:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Helm", GOSSIP_SENDER_MAIN, 241);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Shoulders", GOSSIP_SENDER_MAIN, 242);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Chest", GOSSIP_SENDER_MAIN, 243);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Wrist", GOSSIP_SENDER_MAIN, 244);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Hands", GOSSIP_SENDER_MAIN, 245);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Belt", GOSSIP_SENDER_MAIN, 246);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Legs", GOSSIP_SENDER_MAIN, 247);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Death Knight Boots", GOSSIP_SENDER_MAIN, 248);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_DRUID:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Helm", GOSSIP_SENDER_MAIN, 251);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Shoulders", GOSSIP_SENDER_MAIN, 252);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Chest", GOSSIP_SENDER_MAIN, 253);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Wrist", GOSSIP_SENDER_MAIN, 254);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Hands", GOSSIP_SENDER_MAIN, 255);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Belt", GOSSIP_SENDER_MAIN, 256);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Legs", GOSSIP_SENDER_MAIN, 257);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Druid Boots", GOSSIP_SENDER_MAIN, 258);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_HUNTER:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Helm", GOSSIP_SENDER_MAIN, 261);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Shoulders", GOSSIP_SENDER_MAIN, 262);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Chest", GOSSIP_SENDER_MAIN, 263);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Wrist", GOSSIP_SENDER_MAIN, 264);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Hands", GOSSIP_SENDER_MAIN, 265);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Belt", GOSSIP_SENDER_MAIN, 266);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Legs", GOSSIP_SENDER_MAIN, 267);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Hunter Boots", GOSSIP_SENDER_MAIN, 268);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_ROGUE:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Helm", GOSSIP_SENDER_MAIN, 271);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Shoulders", GOSSIP_SENDER_MAIN, 272);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Chest", GOSSIP_SENDER_MAIN, 273);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Wrist", GOSSIP_SENDER_MAIN, 274);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Hands", GOSSIP_SENDER_MAIN, 275);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Belt", GOSSIP_SENDER_MAIN, 276);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Legs", GOSSIP_SENDER_MAIN, 277);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Rogue Boots", GOSSIP_SENDER_MAIN, 278);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_PRIEST:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Helm", GOSSIP_SENDER_MAIN, 281);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Shoulders", GOSSIP_SENDER_MAIN, 282);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Chest", GOSSIP_SENDER_MAIN, 283);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Wrist", GOSSIP_SENDER_MAIN, 284);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Hands", GOSSIP_SENDER_MAIN, 285);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Belt", GOSSIP_SENDER_MAIN, 286);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Legs", GOSSIP_SENDER_MAIN, 287);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Priest Boots", GOSSIP_SENDER_MAIN, 288);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

		case CLASS_MAGE:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Helm", GOSSIP_SENDER_MAIN, 291);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Shoulders", GOSSIP_SENDER_MAIN, 292);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Chest", GOSSIP_SENDER_MAIN, 293);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Wrist", GOSSIP_SENDER_MAIN, 294);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Hands", GOSSIP_SENDER_MAIN, 295);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Belt", GOSSIP_SENDER_MAIN, 296);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Legs", GOSSIP_SENDER_MAIN, 297);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Mage Boots", GOSSIP_SENDER_MAIN, 298);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;
		}
	}

	void updateDonorPoints(Player * pPlayer, int dp)
	{
		LoginDatabase.PQuery("Update sitez.account_data Set dp = dp + %u WHERE id = '%u'", dp, pPlayer->GetSession()->GetAccountId());
	}

	void refundItem(Player* plr, uint32 itemid, uint32 dp)
	{
		if (plr->HasItemCount(itemid, 1, false))
		{
			plr->DestroyItemCount(itemid, 1, true, false);
			updateDonorPoints(plr, dp);
			ChatHandler(plr->GetSession()).PSendSysMessage("Your item has been refunded.");
		}
		else
		{
			ChatHandler(plr->GetSession()).PSendSysMessage("You do not have that item!");
		}
		plr->PlayerTalkClass->SendCloseGossip();
	}

	bool OnGossipHello(Player* plr, Creature* cre)
	{
		plr->PlayerTalkClass->ClearMenus();
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "|cffff0000WAIT! Before you click anything keep in mind if you refund we will not undo it! So don't Ask!", GOSSIP_SENDER_MAIN, 999);
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "|cffff0000WAIT! Donor + is not refundable once you upgrade you are stuck with it!", GOSSIP_SENDER_MAIN, 999);
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Donor Set |cffff0000(5 DP Each Peice)", GOSSIP_SENDER_MAIN, 101);
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Weapons |cffff0000(10 DP Each)", GOSSIP_SENDER_MAIN, 102);
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Other", GOSSIP_SENDER_MAIN, 103);
		plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
		plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* plr, Creature* cre, uint32 sender, uint32 action)
	{
		if (!sender)
			return false;

		plr->PlayerTalkClass->ClearMenus();

		switch (action)
		{
			// Class Donor Gear
		case 101:
			GetClassRefundMenu(plr, cre);
			break;

			// Weapons
		case 102:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Axe (Lionheart)", GOSSIP_SENDER_MAIN, 301);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Mace (Lionheart)", GOSSIP_SENDER_MAIN, 302);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Sword (Lionheart)", GOSSIP_SENDER_MAIN, 303);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Sword (Thunderfury)", GOSSIP_SENDER_MAIN, 304);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Ashbringer", GOSSIP_SENDER_MAIN, 305);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Bow", GOSSIP_SENDER_MAIN, 307);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Frozen Blood Sword", GOSSIP_SENDER_MAIN, 308);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Sword", GOSSIP_SENDER_MAIN, 309);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Dagger", GOSSIP_SENDER_MAIN, 310);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Sword (Unk)", GOSSIP_SENDER_MAIN, 311);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor 2h Sword", GOSSIP_SENDER_MAIN, 313);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor 2h Axe", GOSSIP_SENDER_MAIN, 314);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donor Shield Melee", GOSSIP_SENDER_MAIN, 315);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Frostmourne", GOSSIP_SENDER_MAIN, 316);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Two-Handed Axe of Strong-WoW", GOSSIP_SENDER_MAIN, 317);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donors Main-Hand Fist of Strong-WoW", GOSSIP_SENDER_MAIN, 318);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donors Off-Hand Fist of Strong-WoW", GOSSIP_SENDER_MAIN, 319);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "DONATE Caster Dagger of Strong-wow", GOSSIP_SENDER_MAIN, 320);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Donors Two-Handed Sword of Strong-WoW", GOSSIP_SENDER_MAIN, 321);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Warglaive of Demonic Intent Main-Hand", GOSSIP_SENDER_MAIN, 322);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Warglaive of Demonic Intent Off-Hand", GOSSIP_SENDER_MAIN, 323);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "DONATE! Off Hand of Strong-WoW Caster", GOSSIP_SENDER_MAIN, 324);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

			// Other
		case 103:
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Refund Transfer Card |cffff0000(40 DP)", GOSSIP_SENDER_MAIN, 401);
			plr->ADD_GOSSIP_ITEM(GOSSIP_ACTION_TRADE, "Exit", GOSSIP_SENDER_MAIN, 999);
			plr->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, cre->GetGUID());
			break;

			// Warrior
		case 201:
			refundItem(plr, 161540, 5);
			break;
		case 202:
			refundItem(plr, 161538, 5);
			break;
		case 203:
			refundItem(plr, 161539, 5);
			break;
		case 204:
			refundItem(plr, 291092, 5);
			break;
		case 205:
			refundItem(plr, 161536, 5);
			break;
		case 206:
			refundItem(plr, 291093, 5);
			break;
		case 207:
			refundItem(plr, 161537, 5);
			break;
		case 208:
			refundItem(plr, 291094, 5);
			break;

			// Paladin
		case 211:
			refundItem(plr, 174555, 5);
			break;
		case 212:
			refundItem(plr, 174553, 5);
			break;
		case 213:
			refundItem(plr, 274554, 5);
			break;
		case 214:
			refundItem(plr, 171089, 5);
			break;
		case 215:
			refundItem(plr, 174551, 5);
			break;
		case 216:
			refundItem(plr, 171090, 5);
			break;
		case 217:
			refundItem(plr, 174552, 5);
			break;
		case 218:
			refundItem(plr, 171091, 5);
			break;

			// Shaman
		case 221:
			refundItem(plr, 164515, 5);
			break;
		case 222:
			refundItem(plr, 164514, 5);
			break;
		case 223:
			refundItem(plr, 164513, 5);
			break;
		case 224:
			refundItem(plr, 171080, 5);
			break;
		case 225:
			refundItem(plr, 164511, 5);
			break;
		case 226:
			refundItem(plr, 271080, 5);
			break;
		case 227:
			refundItem(plr, 164512, 5);
			break;
		case 228:
			refundItem(plr, 171081, 5);
			break;

			// Warlock
		case 231:
			refundItem(plr, 264520, 5);
			break;
		case 232:
			refundItem(plr, 264518, 5);
			break;
		case 233:
			refundItem(plr, 264519, 5);
			break;
		case 234:
			refundItem(plr, 264516, 5);
			break;
		case 235:
			refundItem(plr, 271071, 5);
			break;
		case 236:
			refundItem(plr, 271073, 5);
			break;
		case 237:
			refundItem(plr, 264517, 5);
			break;
		case 238:
			refundItem(plr, 271072, 5);
			break;

			// Dk
		case 241:
			refundItem(plr, 164525, 5);
			break;
		case 242:
			refundItem(plr, 164523, 5);
			break;
		case 243:
			refundItem(plr, 164524, 5);
			break;
		case 244:
			refundItem(plr, 171068, 5);
			break;
		case 245:
			refundItem(plr, 164521, 5);
			break;
		case 246:
			refundItem(plr, 171070, 5);
			break;
		case 247:
			refundItem(plr, 164522, 5);
			break;
		case 248:
			refundItem(plr, 171069, 5);
			break;

			// Druid
		case 251:
			refundItem(plr, 164526, 5);
			break;
		case 252:
			refundItem(plr, 164528, 5);
			break;
		case 253:
			refundItem(plr, 164527, 5);
			break;
		case 254:
			refundItem(plr, 171065, 5);
			break;
		case 255:
			refundItem(plr, 164530, 5);
			break;
		case 256:
			refundItem(plr, 171067, 5);
			break;
		case 257:
			refundItem(plr, 164529, 5);
			break;
		case 258:
			refundItem(plr, 171066, 5);
			break;

			// Hunter
		case 261:
			refundItem(plr, 164535, 5);
			break;
		case 262:
			refundItem(plr, 164533, 5);
			break;
		case 263:
			refundItem(plr, 164534, 5);
			break;
		case 264:
			refundItem(plr, 171087, 5);
			break;
		case 265:
			refundItem(plr, 164531, 5);
			break;
		case 266:
			refundItem(plr, 171086, 5);
			break;
		case 267:
			refundItem(plr, 164532, 5);
			break;
		case 268:
			refundItem(plr, 171088, 5);
			break;

			// Rogue
		case 271:
			refundItem(plr, 164545, 5);
			break;
		case 272:
			refundItem(plr, 164544, 5);
			break;
		case 273:
			refundItem(plr, 164543, 5);
			break;
		case 274:
			refundItem(plr, 171084, 5);
			break;
		case 275:
			refundItem(plr, 164541, 5);
			break;
		case 276:
			refundItem(plr, 171073, 5);
			break;
		case 277:
			refundItem(plr, 164542, 5);
			break;
		case 278:
			refundItem(plr, 171085, 5);
			break;

			// Priest
		case 281:
			refundItem(plr, 164550, 5);
			break;
		case 282:
			refundItem(plr, 164549, 5);
			break;
		case 283:
			refundItem(plr, 164548, 5);
			break;
		case 284:
			refundItem(plr, 171079, 5);
			break;
		case 285:
			refundItem(plr, 164546, 5);
			break;
		case 286:
			refundItem(plr, 171077, 5);
			break;
		case 287:
			refundItem(plr, 164547, 5);
			break;
		case 288:
			refundItem(plr, 171078, 5);
			break;

			// Mage
		case 291:
			refundItem(plr, 164559, 5);
			break;
		case 292:
			refundItem(plr, 164560, 5);
			break;
		case 293:
			refundItem(plr, 164556, 5);
			break;
		case 294:
			refundItem(plr, 171074, 5);
			break;
		case 295:
			refundItem(plr, 164557, 5);
			break;
		case 296:
			refundItem(plr, 171076, 5);
			break;
		case 297:
			refundItem(plr, 164558, 5);
			break;
		case 298:
			refundItem(plr, 171075, 5);
			break;

			// Weapons
		case 301:
			refundItem(plr, 100091, 5);
			break;
		case 302:
			refundItem(plr, 100090, 5);
			break;
		case 303:
			refundItem(plr, 100089, 5);
			break;
		case 304:
			refundItem(plr, 100088, 5);
			break;
		case 305:
			refundItem(plr, 100087, 5);
			break;
		case 307:
			refundItem(plr, 100085, 5);
			break;
		case 308:
			refundItem(plr, 100084, 5);
			break;
		case 309:
			refundItem(plr, 100083, 5);
			break;
		case 310:
			refundItem(plr, 1000001, 5);
			break;
		case 311:
			refundItem(plr, 100081, 5);
			break;
		case 312:
			refundItem(plr, 100080, 5);
			break;
		case 313:
			refundItem(plr, 100124, 5);
			break;
		case 314:
			refundItem(plr, 100122, 5);
			break;
		case 315:
			refundItem(plr, 100130, 5);
			break;
		case 316:
			refundItem(plr, 36942, 40);
			break;
		case 317:
			refundItem(plr, 494623, 40);
			break;
		case 318:
			refundItem(plr, 342331, 40);
			break;
		case 319:
			refundItem(plr, 342203, 40);
			break;
		case 320:
			refundItem(plr, 193474, 40);
			break;
		case 321:
			refundItem(plr, 507761, 40);
			break;
		case 322:
			refundItem(plr, 662234, 40);
			break;
		case 323:
			refundItem(plr, 662233, 40);
			break;
		case 324:
			refundItem(plr, 337636, 40);
			break;

			// Other
		case 401:
			refundItem(plr, 999999, 40);
			break;
		
			// Exit
		case 999:
			plr->PlayerTalkClass->SendCloseGossip();
			break;
		}
		return true;
	}
};

void AddSC_taco_refund_npc()
{
	new taco_refund_npc();
}
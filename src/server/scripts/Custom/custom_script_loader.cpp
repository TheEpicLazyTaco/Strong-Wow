/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:
//void AddSC_Boss_Announcer();
void AddSC_DPSystem();
void AddSC_npc_tic_tac_toe();
void AddSC_PortableTeleporter_ItemGossip();
void AddSC_Professions_NPC();
void AddSC_StrongWow_Tools();
void AddSC_onlogin_announcer();
void AddSC_cs_world_chat();
void SetupKillStreak();
void AddSC_dp_changer();
void AddSC_Duel_Reset();
//void AddSC_hva_commandscript();
void AddSC_Npc_Beastmaster();
void AddSC_Professions_NPC();
void AddSC_npc_trainer();
void AddSC_npc_supporter();
void AddSC_npc_enchantment();
void AddSC_item_custom_summon();
void AddSC_gamble_npc();
void AddSC_npc_changer();
void AddSC_vip_token_item1();
void AddSC_TitleNpc();
void AddSC_voterewarder();
void AddSC_custom_LevelAnnounce();
void AddSC_REFORGER_NPC();
void AddSC_refundvendor();
void AddSC_zombie_event();
void AddSC_donorrewarder();
void AddSC_Vip_Access();
void AddSC_npc_1v1arena();
void AddSC_casino();
void AddSC_dp_token_item();
void AddSC_player_reward();
void AddSC_npc_donation_refunder();
void AddSC_Professions_NPC2();
void AddSC_taco_refund_npc();
void AddSC_aprilFools();
void AddSC_trainer();
void AddSC_PWS_Transmogrification();
void AddSC_CS_Transmogrification();
void AddSC_NPC_TransmogDisplayVendor();
void AddSC_first_login();
void AddSC_PvPRewards();
// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddCustomScripts()
{
	//AddSC_Boss_Announcer();
	AddSC_DPSystem();
	AddSC_npc_enchantment();
	AddSC_npc_tic_tac_toe();
	AddSC_Professions_NPC();
	AddSC_Professions_NPC2();
	AddSC_StrongWow_Tools();
	AddSC_onlogin_announcer();
	AddSC_cs_world_chat();
	SetupKillStreak();
	AddSC_CS_Transmogrification();
	AddSC_PWS_Transmogrification();
	AddSC_NPC_TransmogDisplayVendor();
	AddSC_PortableTeleporter_ItemGossip();
	AddSC_TitleNpc();
	AddSC_vip_token_item1();
	AddSC_item_custom_summon();
	AddSC_npc_trainer();
	AddSC_npc_supporter();
	AddSC_Npc_Beastmaster();
	AddSC_Duel_Reset();
	AddSC_gamble_npc();
	AddSC_npc_changer();
	AddSC_dp_changer();
	AddSC_custom_LevelAnnounce();
	AddSC_voterewarder();
	AddSC_REFORGER_NPC();
	AddSC_donorrewarder();
	AddSC_refundvendor();
	AddSC_zombie_event();
	AddSC_Vip_Access();
	AddSC_npc_1v1arena();
	AddSC_casino();
	AddSC_dp_token_item();
	AddSC_player_reward();
	AddSC_npc_donation_refunder();
	AddSC_taco_refund_npc();
	AddSC_aprilFools();
	AddSC_trainer();
	AddSC_first_login();
	AddSC_PvPRewards();
}
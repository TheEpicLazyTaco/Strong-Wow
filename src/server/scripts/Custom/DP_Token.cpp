#include "ScriptMgr.h"
#include "Item.h"
#include "Player.h"

class dp_token_item : public ItemScript
{
public: dp_token_item() : ItemScript("dp_token_item") { }
       
	bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/)
	{
		if (player->HasItemCount(item->GetEntry(), 1, true))
					player->DestroyItemCount(item->GetEntry(), 1, true, false);

		LoginDatabase.PExecute("UPDATE sitez.account_data SET DP = DP + 1 WHERE id = '%u'", player->GetSession()->GetAccountId());
		ChatHandler(player->GetSession()).PSendSysMessage("[Strong-WoW]You have gained 1 dp!");
		return true;
	}
};
 
void AddSC_dp_token_item()
{
	new dp_token_item();
}
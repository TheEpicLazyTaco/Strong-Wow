class PvPRewards : PlayerScript
{
public:
	PvPRewards() : PlayerScript("PvPRewards") {}

	// ~ Setting Variables "Not Ideal But it will have to do."  ~
	
	// The item you want to give players.
	uint32 itemID = 353533;
	// Ammount of items to give.
	uint32 ammount = 2;
	// Time in seconds 60 = 1 Minute
	uint32 cooldown = 300;
	// This is the message that is sent when a player does recieve a reward.
	std::string rewardMessage = "|cffff0000[System]|r You recieved a reward and are now on a cooldown.";
	// Thhis is the message that is sent when a player is on cooldown.
	std::string cooldownMessage = "|cffff0000[System]|r You are still on a cooldown and can not recieve a reward.";

	//IMPORTANT! Unless you know what your doing I do not recommend editing anything below this comment.

	// This is the map that is going to store all of our player's cooldowns during game play.
	std::unordered_map <uint32, uint32>  cooldownList;

	// Returns our cooldown from the map if it has one.
	uint32 GetPlayerCD(std::unordered_map<uint32, uint32> m, uint32 key)
	{
		//This gets our player's cooldown from the map and returns it to us in a uint32 form.
		auto itr = m.find(key);

		if (itr != m.end())
			return itr->second;

		//This is mainly to make the compiler happy about not all paths returning a value but also serves 
		return NULL;
	}

	//Updates/Inserts our players unique identifier and their cooldown.
	void UpdatePlayerCD(std::unordered_map<uint32, uint32>& m, uint32 key, uint32 newValue)
	{
		//If it finds a value update it with new value
		auto itr = m.find(key);
		if (itr != m.end()) {
			m[key] = newValue;
		}
		//If that character is not in the map add it.
		else
			m.insert(std::make_pair(key, newValue));
	}

	//This is where we add our item and send a mssg to our player and then we update the map.
	void AddReward(Player * killer, uint32 itemId, uint32 ammount, uint32 cooldown)
	{
		//Add the item to the player.
		killer->AddItem(itemId, ammount);
		//Send Message.
		ChatHandler(killer->GetSession()).PSendSysMessage(rewardMessage.c_str());
		//Inset Data into map.
		UpdatePlayerCD(cooldownList, killer->GetGUID().GetCounter(), killer->GetTotalPlayedTime() + cooldown);
	}

	//This is called when a player kills another player.
	void OnPVPKill(Player* killer, Player* /*killed*/)
	{
		//Variable to store the saved cooldown value we get from our map.
		uint32 mapCD;

		//If the map is empty.
		if (cooldownList.empty())
			mapCD = NULL;
		//Get the cd from the map and stores it in mapCD.
		else
			mapCD = GetPlayerCD(cooldownList, killer->GetGUID().GetCounter());
		
		//If mapCD has no value.
		if (mapCD == NULL)
		{
			AddReward(killer, itemID, ammount, cooldown);
		}
		//If mapCD has a value.
		else
		{
			// If player's played time is higher then the cooldown time.
			if (killer->GetTotalPlayedTime() >= mapCD) {
				// Add items, Send Message, and Update Map. ~ See Function For More Info ~
				AddReward(killer, itemID, ammount, cooldown);
			}
			else {
				// If you are still on cooldown send cooldownMessage.
				ChatHandler(killer->GetSession()).PSendSysMessage(cooldownMessage.c_str());
			}
		}
	}
};

void AddSC_PvPRewards()
{
	new PvPRewards();
}
/*
*        _             _          ____        _                  _
*       | |           | |        |  _ \      | |                (_)
*       | |_   _ _ __ | | ___   _| |_) |_   _| | __ _  __ _ _ __ _  __ _
*   _   | | | | | '_ \| |/ / | | |  _ <| | | | |/ _` |/ _` | '__| |/ _` |
*  | |__| | |_| | | | |   <| |_| | |_) | |_| | | (_| | (_| | |  | | (_| |
*   \____/ \__,_|_| |_|_|\_\\__, |____/ \__,_|_|\__, |\__,_|_|  |_|\__,_|
*                            __/ |               __/ |
*                           |___/               |___/
*   Copyright © JunkyBulgaria a.k.a Emiliyan Kartselyanski 2017
*/
#include "AccountMgr.h"
#include "ScriptMgr.h"


#define TOKEN_REQUIRED 112311 // 112311
#define TOKEN_REQUIRED_COUNT 1
#define ICON_HEIGHT 35 // height of the icon from gossip menu
#define ICON_WIDTH 35 // width of the icon from gossip menu
#define MAX_ITEM_ENTRIES 99 // from 0 to 96 = 97, if you add more items = increase this to their total count

// gossip should be declared manually for these two
#define ARMOR_REFUND_POINTS 10 // how much points to refund from armor items
#define WEAPON_REFUND_POINTS 15 // how much points to refund from weapon items



uint32 armor[] = {
	// armor
	161536, 161537, 161538, 161539, 161540, 291093, 291094, 291092, 164515, 164514, 164512, 164513, 164511, 271080, 171081, 171080,
	164516, 164517, 164518, 164519, 164520, 171071, 171073, 171072, 164521, 164522, 164523, 164524, 164525, 171068, 171069, 171070,
	164526, 164527, 164528, 164529, 164530, 171065, 171066, 171067, 164531, 164532, 164533, 164534, 164535, 171086, 171087, 171088,
	164541, 164542, 164543, 164544, 164545, 171073, 171084, 171085, 164546, 164547, 164548, 164549, 164550, 171077, 171078, 171079,
	164556, 164557, 164558, 164559, 164560, 171074, 171075, 171076, 171089, 171090, 171091, 174551, 174552, 174553, 174554, 174555, 999999,
	// weapons
	100092, 100091, 100090, 100089, 100088, 100087, 100086, 100085, 100084, 100083, 100082, 100081, 100080, 100124, 100122, 100130
};

class npc_donation_refunder : public CreatureScript
{
public:
	npc_donation_refunder() : CreatureScript("npc_donation_refunder") { }

private:
	const char * GetSlotName(uint8 slot, WorldSession* session)
	{
		switch (slot)
		{
		case EQUIPMENT_SLOT_HEAD: return "Head";
		case EQUIPMENT_SLOT_SHOULDERS: return "Shoulders";
		case EQUIPMENT_SLOT_BODY: return "Shirt";
		case EQUIPMENT_SLOT_CHEST: return "Chest";
		case EQUIPMENT_SLOT_WAIST: return "Waist";
		case EQUIPMENT_SLOT_LEGS: return "Legs";
		case EQUIPMENT_SLOT_FEET: return "Boots";
		case EQUIPMENT_SLOT_WRISTS: return "Wrists";
		case EQUIPMENT_SLOT_HANDS: return "Hands";
		case EQUIPMENT_SLOT_BACK: return "Cloak";
		case EQUIPMENT_SLOT_MAINHAND: return "Main Hand";
		case EQUIPMENT_SLOT_OFFHAND: return "Off Hand";
		case EQUIPMENT_SLOT_RANGED: return "Ranged";
		case EQUIPMENT_SLOT_TABARD: return "Tabard";
		default: return NULL;
		}
	}

	uint32 limit = 0;

	std::string GetItemIcon(uint32 entry, uint32 width, uint32 height, int x, int y)
	{
		std::ostringstream ss;
		ss << "|TInterface";
		const ItemTemplate* temp = sObjectMgr->GetItemTemplate(entry);
		const ItemDisplayInfoEntry* dispInfo = NULL;
		if (temp)
		{
			dispInfo = sItemDisplayInfoStore.LookupEntry(temp->DisplayInfoID);
			if (dispInfo)
				ss << "/ICONS/" << dispInfo->inventoryIcon;
		}
		if (!temp && !dispInfo)
			ss << "/InventoryItems/WoWUnknownItem01";
		ss << ":" << width << ":" << height << ":" << x << ":" << y << "|t";
		return ss.str();
	}

	std::string GetSlotIcon(uint8 slot, uint32 width, uint32 height, int x, int y)
	{
		std::ostringstream ss;
		ss << "|TInterface/PaperDoll/";
		switch (slot)
		{
		case EQUIPMENT_SLOT_HEAD: ss << "UI-PaperDoll-Slot-Head"; break;
		case EQUIPMENT_SLOT_SHOULDERS: ss << "UI-PaperDoll-Slot-Shoulder"; break;
		case EQUIPMENT_SLOT_BODY: ss << "UI-PaperDoll-Slot-Shirt"; break;
		case EQUIPMENT_SLOT_CHEST: ss << "UI-PaperDoll-Slot-Chest"; break;
		case EQUIPMENT_SLOT_WAIST: ss << "UI-PaperDoll-Slot-Waist"; break;
		case EQUIPMENT_SLOT_LEGS: ss << "UI-PaperDoll-Slot-Legs"; break;
		case EQUIPMENT_SLOT_FEET: ss << "UI-PaperDoll-Slot-Feet"; break;
		case EQUIPMENT_SLOT_WRISTS: ss << "UI-PaperDoll-Slot-Wrists"; break;
		case EQUIPMENT_SLOT_HANDS: ss << "UI-PaperDoll-Slot-Hands"; break;
		case EQUIPMENT_SLOT_BACK: ss << "UI-PaperDoll-Slot-Chest"; break;
		case EQUIPMENT_SLOT_MAINHAND: ss << "UI-PaperDoll-Slot-MainHand"; break;
		case EQUIPMENT_SLOT_OFFHAND: ss << "UI-PaperDoll-Slot-SecondaryHand"; break;
		case EQUIPMENT_SLOT_RANGED: ss << "UI-PaperDoll-Slot-Ranged"; break;
		case EQUIPMENT_SLOT_TABARD: ss << "UI-PaperDoll-Slot-Tabard"; break;
		default: ss << "UI-Backpack-EmptySlot";
		}
		ss << ":" << width << ":" << height << ":" << x << ":" << y << "|t";
		return ss.str();
	}

	bool OnGossipHello(Player* player, Creature* _creature)
	{
		if (player->HasItemCount(TOKEN_REQUIRED, TOKEN_REQUIRED_COUNT, false))
			LoadItemsFromCharacterAndAddToGossip(player);
		else
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "You need our refund token before you try to refund your item.", GOSSIP_SENDER_MAIN, 1);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
		return true;
	}

	bool IsEligibleForRefund(Item* item)
	{
		for (uint32 i = 0; i <= MAX_ITEM_ENTRIES; i++) {
			if (item->GetEntry() == armor[i])
				return true;
			else
				return false;
		}
		return false;
	}

	void LoadItemsFromCharacterAndAddToGossip(Player* player)
	{
		WorldSession* session = player->GetSession();

		// load items from character panel
		for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
		{
			if (limit >= 28)
				break;

			if (Item* newItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
				if (IsEligibleForRefund(newItem))
				{
					limit++;
					std::string icon = GetItemIcon(newItem->GetEntry(), ICON_WIDTH, ICON_HEIGHT, -18, 0);
					// if weapon = 15 dp
					if (newItem->GetTemplate()->InventoryType == 13 || newItem->GetTemplate()->InventoryType == 14 || newItem->GetTemplate()->InventoryType == 15 ||
						newItem->GetTemplate()->InventoryType == 17 || newItem->GetTemplate()->InventoryType == 21 || newItem->GetTemplate()->InventoryType == 22 ||
						newItem->GetTemplate()->InventoryType == 23 || newItem->GetTemplate()->InventoryType == 24 || newItem->GetTemplate()->InventoryType == 25 ||
						newItem->GetTemplate()->InventoryType == 26 || newItem->GetTemplate()->InventoryType == 28)
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, icon + "[|cffFF0000 15 DP|r ]" + newItem->GetTemplate()->Name1, GOSSIP_SENDER_MAIN, newItem->GetEntry());
					else // if armor or smth else = 10 dp
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, icon + "[|cffFF0000 10 DP|r ]" + newItem->GetTemplate()->Name1, GOSSIP_SENDER_MAIN, newItem->GetEntry());
				}
		}

		// load items in inventory
		for (uint8 i = INVENTORY_SLOT_BAG_START; i < INVENTORY_SLOT_BAG_END; i++)
		{
			if (Bag* bag = player->GetBagByPos(i))
			{
				for (uint32 j = 0; j < bag->GetBagSize(); j++)
				{
					if (limit >= 28)
						break;

					if (Item* newItem = player->GetItemByPos(i, j))
					{
						TC_LOG_INFO("server.worldserver", "FOUND ITEM");
						uint32 display = newItem->GetTemplate()->DisplayInfoID;
						limit++;
						std::string icon = GetItemIcon(newItem->GetEntry(), ICON_WIDTH, ICON_HEIGHT, -18, 0);
						// if weapon = 15 dp
						if (newItem->GetTemplate()->InventoryType == 13 || newItem->GetTemplate()->InventoryType == 14 || newItem->GetTemplate()->InventoryType == 15 ||
							newItem->GetTemplate()->InventoryType == 17 || newItem->GetTemplate()->InventoryType == 21 || newItem->GetTemplate()->InventoryType == 22 ||
							newItem->GetTemplate()->InventoryType == 23 || newItem->GetTemplate()->InventoryType == 24 || newItem->GetTemplate()->InventoryType == 25 ||
							newItem->GetTemplate()->InventoryType == 26 || newItem->GetTemplate()->InventoryType == 28)
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, icon + "[|cffFF0000 15 DP|r ]" + newItem->GetTemplate()->Name1, GOSSIP_SENDER_MAIN, newItem->GetEntry());
						else // if armor or smth else = 10 dp
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, icon + "[|cffFF0000 10 DP|r ]" + newItem->GetTemplate()->Name1, GOSSIP_SENDER_MAIN, newItem->GetEntry());
					}
				}
			}
		}
	}

	void RefundItems(uint32 ItemId, uint8 points, Player* player)
	{
		bool eligiblex = true;
		std::string accountName = "";
		WorldSession * m_session = player->GetSession();
		AccountMgr::GetName(player->GetSession()->GetAccountId(), accountName);
		uint32 accID = player->GetSession()->GetAccountId();
		TC_LOG_INFO("server.worldserver", "[Donation Refund NPC] ACCOUNT NAME: %s", accountName);

		// check if player has account in website
		QueryResult result = LoginDatabase.PQuery("select dp from sitez.account_data where id = '%u'", accID);
		if (!result)
		{
			player->CLOSE_GOSSIP_MENU();
			m_session->SendNotification("Your account is not linked with our website, please contact our team for support!");
			eligiblex = false;
		}

		QueryResult select_dp_result = LoginDatabase.PQuery("select dp from sitez.account_data where id = '%u'", accID);
		Field* fields = select_dp_result->Fetch();
		uint32 DP = fields[0].GetUInt32();

		if (player->HasItemCount(ItemId, 1, false) && eligiblex)
		{
			LoginDatabase.PExecute("update sitez.account_data set dp = dp + %u where user = '%u'", points, accID);
			m_session->SendNotification("%u Donation Points have been refunded to your account!", points);
			player->DestroyItemCount(ItemId, 1, true); // remove donator item
			player->DestroyItemCount(TOKEN_REQUIRED, TOKEN_REQUIRED_COUNT, true); // remove tokens
			player->CLOSE_GOSSIP_MENU();
			TC_LOG_INFO("server.worldserver", "[Donation Refund NPC] REFUND ITEM SUCCESS: %u, POINTS: %u to player: %s and his guid: %u", ItemId, points, player->GetName().c_str(), player->GetGUID());
		}
		else
		{
			m_session->SendNotification("You don't have this item!");
			player->CLOSE_GOSSIP_MENU();
			TC_LOG_INFO("server.worldserver", "[Donation Refund NPC] REFUND ITEM FAIL: %u, POINTS: %u to player: %s and his guid: %u", ItemId, points, player->GetName().c_str(), player->GetGUID());
		}
		player->PlayerTalkClass->SendCloseGossip();
	}

	bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
	{
		if (sender == GOSSIP_SENDER_MAIN)
		{
			player->PlayerTalkClass->ClearMenus();
			switch (uiAction)
			{
				// armor
			case 161536:
			case 161537:
			case 161538:
			case 161539:
			case 161540:
			case 291093:
			case 291094:
			case 291092:
			case 164515:
			case 164514:
			case 164512:
			case 164513:
			case 164511:
			case 271080:
			case 171081:
			case 171080:
			case 164516:
			case 164517:
			case 164518:
			case 164519:
			case 164520:
			case 171071:
			case 171073:
			case 171072:
			case 164521:
			case 164522:
			case 164523:
			case 164524:
			case 164525:
			case 171068:
			case 171069:
			case 171070:
			case 164526:
			case 164527:
			case 164528:
			case 164529:
			case 164530:
			case 171065:
			case 171066:
			case 171067:
			case 164531:
			case 164532:
			case 164533:
			case 164534:
			case 164535:
			case 171086:
			case 171087:
			case 171088:
			case 164541:
			case 164542:
			case 164543:
			case 164544:
			case 164545:
			case 171084:
			case 171085:
			case 164546:
			case 164547:
			case 164548:
			case 164549:
			case 164550:
			case 171077:
			case 171078:
			case 171079:
			case 164556:
			case 164557:
			case 164558:
			case 164559:
			case 164560:
			case 171074:
			case 171075:
			case 171076:
			case 171089:
			case 171090:
			case 171091:
			case 174551:
			case 174552:
			case 174553:
			case 174554:
			case 174555:
			case 999999:
				RefundItems(uiAction, ARMOR_REFUND_POINTS, player);
				break;

				// weapons
			case 100092:
			case 100091:
			case 100090:
			case 100089:
			case 100088:
			case 100087:
			case 100086:
			case 100085:
			case 100084:
			case 100083:
			case 100082:
			case 100081:
			case 100080:
			case 100124:
			case 100122:
			case 100130:
				RefundItems(uiAction, WEAPON_REFUND_POINTS, player);
				break;
			}
		}
		return true;
	}
};

void AddSC_npc_donation_refunder()
{
	new npc_donation_refunder();
}
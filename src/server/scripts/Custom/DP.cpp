#include "ScriptPCH.h"
 
 
class dp_changer : public CreatureScript
{
public: dp_changer() : CreatureScript("dp_changer") { }
 
                uint32 GetVP(Player* player)
                {
                        uint32 points;
 
                        QueryResult result = LoginDatabase.PQuery("SELECT vp FROM sitez.account_data WHERE id = '%u' AND vp >= '0'", player->GetSession()->GetAccountId());
                        if (result)
                        {
                                do
                                {
                                        points = result->Fetch()->GetInt32();
 
                                } while (result->NextRow());
                        }
                        else
                                points = 0;
 
                        return points;
                }
 
                uint32 GetDP(Player* player)
                {
                        uint32 points;
 
                        QueryResult result = LoginDatabase.PQuery("SELECT dp FROM sitez.account_data WHERE id = '%u' AND dp >= '0'", player->GetSession()->GetAccountId());
                        if (result)
                        {
                                do
                                {
                                        points = result->Fetch()->GetInt32();
 
                                } while (result->NextRow());
                        }
                        else
                                points = 0;
 
                        return points;
                }
 
                bool ExchangeToVP(Player* player, uint32 dp, uint32 vp)
                {
                        if (GetDP(player) < dp)
                                return false;
 
                        LoginDatabase.PQuery("Update sitez.account_data Set dp = dp - '%u' WHERE id = '%u'", dp, player->GetSession()->GetAccountId());
 
                        LoginDatabase.PQuery("Update sitez.account_data Set vp = vp + '%u' WHERE id = '%u'", vp, player->GetSession()->GetAccountId());
 
                        return true;
                }
 
                bool ExchangeToDP(Player* player, uint32 vp, uint32 dp)
                {
                        if (GetVP(player) < vp)
                                return false;
 
                        LoginDatabase.PQuery("Update sitez.account_data Set vp = vp - '%u' WHERE id = '%u'", vp, player->GetSession()->GetAccountId());
 
                        LoginDatabase.PQuery("Update sitez.account_data Set dp = dp + '%u' WHERE id = '%u'", dp, player->GetSession()->GetAccountId());
 
                        return true;
                }
 
                bool OnGossipHello(Player* player, Creature* pCreature)
                {
 
                        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|cff00ff00|TInterface\\icons\\spell_holy_fanaticism:30|t|r Exchange 150VP to 1DP ", GOSSIP_SENDER_MAIN, 1, "|cFF8B0000 Change 150VP to 1DP|r", 0, false);
                        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|cff00ff00|TInterface\\icons\\spell_holy_fanaticism:30|t|r Exchange 1DP to 100VP ", GOSSIP_SENDER_MAIN, 2, "|cFF8B0000 1DP to 100VP|r", 0, false);
 
                        player->PlayerTalkClass->SendGossipMenu(85008, pCreature->GetGUID());
                        return true;
                }
 
                void ReturnToMainMenu(Player* player, Creature* creature)
                {
                        player->PlayerTalkClass->ClearMenus();
                        OnGossipHello(player, creature);
                }
 
                char str[255];
 
                bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction)
                {
                        player->PlayerTalkClass->ClearMenus();
 
                        switch (uiAction)
                        {
                        case 0:
                                ReturnToMainMenu(player, creature);
                                break;
                        case 1:
                                if (!ExchangeToDP(player, 150, 1))
                                        sprintf(str, "You dont have enough vote points");
                                else
                                        sprintf(str, "You have changed 150VP to 1DP, congratulations");
 
                                player->GetSession()->SendNotification(str, player->GetGUID(), true);
                                ReturnToMainMenu(player, creature);
                                break;
                               
            case 2:
                                if (!ExchangeToVP(player, 1, 100))
                                        sprintf(str, "You dont have enough DP points");
                                else
                                        sprintf(str, "You have changed 1DP to 100VP, congratulations");
 
                                player->GetSession()->SendNotification(str, player->GetGUID(), true);
                                ReturnToMainMenu(player, creature);
                                break;
 
                        }
                        return true;
                }
};
 
void AddSC_dp_changer()
{
        new dp_changer();
}
#include "ScriptMgr.h"
#include "Player.h"
#include "Chat.h"

class welcome_player_message : public PlayerScript
{
public:
    welcome_player_message() : PlayerScript("welcome_player_message") { }


	std::string race(Player* player)
	{
		switch (player->getRace())
		{
		case 1:
			return "Human ";
			break;

		case 2:
			return "Orc ";
			break;

		case 3:
			return "Drawf ";
			break;

		case 4:
			return "Night Elf ";
			break;

		case 5: 
			return "Undead ";
			break;

		case 6:
			return "Tauren ";
			break;

		case 7:
			return "Gnome ";
			break;

		case 8:
			return "Troll ";
			break;

		case 10:
			return "Blood Elf ";
			break;

		case 11:
			return "Draenei ";
			break;
		}
		return "";
	}

	std::string _class(Player * player)
	{
		switch (player->getClass())
		{
		case CLASS_WARRIOR:
			return "|cff804040Warrior|r ";
			break;

		case CLASS_PALADIN:
			return "|cffFF00FFPaladin|r ";
			break;

		case CLASS_HUNTER:
			return "|cff008000Hunter|r ";
			break;

		case CLASS_ROGUE:
			return "|cffFFFF80Rogue|r ";
			break;

		case CLASS_PRIEST:
			return "|cffFFFFFFPriest|r ";
			break;

		case CLASS_DEATH_KNIGHT:
			return "|cffFF0000Death Knight|r ";
			break;

		case CLASS_SHAMAN:
			return "|cff0000FFShaman|r ";
			break;

		case CLASS_MAGE:
			return "|cffC0C0FFMage|r ";
			break;

		case CLASS_WARLOCK:
			return "|cff8080FFWarlock|r ";
			break;

		case CLASS_DRUID:
			return "|cffFFC080Druid|r ";
			break;
		}
		return "";
	}

	std::string faction(Player * player)
	{
		switch (player->GetTeam())
		{
		case ALLIANCE:
			return "|cff0000FFAlliance|r";
			break;

		case HORDE:
			return "|cffFF0000Horde|r";
			break;
		}
		return "";
	}

	void OnLogin(Player* player, bool firstLogin) override
	{
		std::ostringstream message;

		if (firstLogin)
		{
			message << "Hello! I'm " << player->GetName() << " a " << race(player) << _class(player) << "for the " << faction(player) << ". I'm a new player. It is nice to meet you all!";
			sWorld->SendGlobalText(message.str().c_str(), nullptr);
		}
		else
		{
			message << "|cffFF0000Welcome to back Strong-WoW " << player->GetName() << "!|r";
			ChatHandler(player->GetSession()).PSendSysMessage(message.str().c_str());
			
			// Titan's Grip
			if (player->getClass() == CLASS_DRUID)
			{
				if (player->HasSpell(46917))
					player->RemoveSpell(46917, false, false);
				if (player->HasSpell(49152))
					player->RemoveSpell(49152, false, false);
			}
		}
    }
};

void AddSC_onlogin_announcer()
{
    new welcome_player_message();
}
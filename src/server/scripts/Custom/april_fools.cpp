#include "ScriptPCH.h"

class aprilFools : public CreatureScript
{
public:
	aprilFools() : CreatureScript("aprilFools") {}

	bool OnGossipHello(Player * pPlayer, Creature * pCreature)
	{
		pPlayer->ADD_GOSSIP_ITEM(0, "|cffd62e93You want free stuff? Click Here!", GOSSIP_SENDER_MAIN, 100);
		pPlayer->ADD_GOSSIP_ITEM(0, "|cffd62e93You want free stuff? Or Here!", GOSSIP_SENDER_MAIN, 101);
		pPlayer->ADD_GOSSIP_ITEM(0, "|cffd62e93You want free stuff? Or Maybe Here!", GOSSIP_SENDER_MAIN, 100);
		pPlayer->ADD_GOSSIP_ITEM(0, "|cffd62e93You want free stuff? Perhaps Here!", GOSSIP_SENDER_MAIN, 101);
		pPlayer->ADD_GOSSIP_ITEM(0, "|cffd62e93You want free stuff? Definitely Not Here!", GOSSIP_SENDER_MAIN, 100);
		pPlayer->SEND_GOSSIP_MENU(1, pCreature->GetGUID());
		return true;
	}


	bool OnGossipSelect(Player * pPlayer, Creature * Creature, uint32 /*uiSender*/, uint32 uiAction)
	{
		pPlayer->PlayerTalkClass->ClearMenus();

		std::string msg;
		switch (uiAction)
		{
			case 100:
				Creature->CastSpell(pPlayer, 6754, false, 0, 0, Creature->GetGUID());
				msg += "|cffd62e93[April Fools] ";
				msg += pPlayer->GetName();
				msg += " has been tricked!";
				sWorld->SendGlobalText(msg.c_str(), nullptr);
				//sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), pPlayer);
				pPlayer->PlayerTalkClass->SendCloseGossip();
				break;

			case 101:
				Creature->CastSpell(pPlayer, 6754, false, 0, 0, Creature->GetGUID());
				msg += "|cffd62e93[April Fools] ";
				msg += pPlayer->GetName();
				msg += " has been tricked!";
				sWorld->SendGlobalText(msg.c_str(), nullptr);
				//sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), pPlayer);
				pPlayer->PlayerTalkClass->SendCloseGossip();
				break;
		}
		return true;
	}
};

void AddSC_aprilFools()
{
	new aprilFools();
}
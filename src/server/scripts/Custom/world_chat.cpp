#include "ScriptPCH.h"
#include "Chat.h"
#include "Common.h"
#include "Channel.h"
#include "SocialMgr.h"

class cs_world_chat : public CommandScript
{
public:
	cs_world_chat() : CommandScript("cs_world_chat") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> commandTable =
		{
			// .c .ch .cha .chat
			{ "c", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "ch", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "cha", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "chat", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			// .w .wo .wor .worl .world
			{ "w", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "wo", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "wor", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "worl", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "world", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			// .g .gl .glo .glob .globa .global
			{ "g", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "gl", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "glo", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "glob", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "globa", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
			{ "global", rbac::RBAC_PERM_COMMAND_CUSTOM_CHAT, false, &HandleGlobalChat, "" },
		};

		return commandTable;
	}

	static bool HandleGlobalChat(ChatHandler* handler, const char* args)
	{
		Player * player = handler->GetSession()->GetPlayer();

		// Player Muted?
		if (!player->CanSpeak())
			return false;

		// Other Check.
		std::string temp = args;

		if (!args || temp.find_first_not_of(' ') == std::string::npos)
			return false;

		//Item Link Fix.
		std::string color_replace = player->factionColor(player);
		player->stringReplace(temp, "|r", color_replace);

		// Start Building The String.
		std::string msg = "";

		// GM?
		if (!player->GetSession()->GetSecurity() == 0 && player->isGMChat())
			msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:12:22:0:-2|t ";

		msg += "|cff53ee5e[World]|r";

		//Vip
		if (player->GetSession()->GetSecurity() == 1)
			msg += "[VIP] ";

		// Factionicon
		msg += player->factionIcon(player);

		//RaceIcon
		msg += player->RaceIcon(player);

		// Name?
		msg += handler->GetNameLink();
		msg += " ";

		// Faction?
		msg += player->factionColor(player);

		// Rebuilt String / What ever the player has to say.
		msg += temp;

		// Ignore Check & Then Send Message.
		SessionMap sessions = sWorld->GetAllSessions();
		for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
			if (Player* plr = itr->second->GetPlayer())
				if (!plr->GetSocial()->HasIgnore(player->GetGUID()))
					sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);
		return true;
	}
};

void AddSC_cs_world_chat()
{
	new cs_world_chat;
}
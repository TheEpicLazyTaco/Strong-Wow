class trainer : CreatureScript
{
	public:
		trainer() : CreatureScript("trainer") {}
	

	// Stop me from repeating myself.
	void TeachSpells(Player * player, uint32 spellArray[], uint32 spellArrayCount)
	{
		for (size_t i = 0; i < spellArrayCount; i++)
			player->LearnSpell(spellArray[i], false);
	}

	virtual bool OnGossipHello(Player* player, Creature* creature) 
	{
		player->ADD_GOSSIP_ITEM(3, "|TInterface/ICONS/Spell_Arcane_StudentOfMagic:30:30:-21:0|tTrain Me", GOSSIP_SENDER_MAIN, 101);
		player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\spell_chargenegative:30:30:-21:0|tExit", GOSSIP_SENDER_MAIN, 102);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true; 
	}

	virtual bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) 
	{
		// Sender Check
		if (!sender == GOSSIP_SENDER_MAIN)
			return false;

		// Clear Menus
		player->PlayerTalkClass->ClearMenus();

		// Spell Arrays.
		uint32 Warrior_Spells[] = { 47436, 47450, 11578, 47465, 47502, 34428, 1715, 3127, 7384, 2687, 71, 7386, 355, 72, 47437, 57823, 694, 2565, 676, 47520, 20230, 12678, 47471, 1161, 871, 2458, 20252, 47475, 18499, 1680, 6552, 47488, 1719, 23920, 47440, 3411, 64382, 55694, 57755, 674, 750, 5246 };
		uint32 Paladin_Spells[] = { 19752, 3127, 19746, 750, 48942, 48782, 48932, 20271, 498, 10308, 1152, 10278, 48788, 53408, 48950, 48936, 31789, 62124, 54043, 25780, 1044, 20217, 48819, 48801, 48785, 5502, 20164, 10326, 1038, 53407, 48943, 20165, 48945, 642, 48947, 20166, 4987, 48806, 6940, 48817, 48934, 48938, 25898, 25899, 32223, 31884, 54428, 61411, 53601, 33388, 33391 };
		uint32 A_Paladin_Spells[] = { 31801, 13819, 23214 };
		uint32 H_Paladin_Spells[] = { 53736, 34769, 34767 };
		uint32 Hunter_Spells[] = { 3127, 674, 3043, 3045, 8737, 1494, 13163, 48996, 49001, 49045, 53338, 5116, 27044, 883, 2641, 6991, 982, 1515, 19883, 20736, 48990, 2974, 6197, 1002, 14327, 5118, 49056, 53339, 49048, 19884, 34074, 781, 14311, 1462, 19885, 19880, 13809, 13161, 5384, 1543, 19878, 49067, 3034, 13159, 19882, 58434, 49071, 49052, 19879, 19263, 19801, 34026, 34600, 34477, 61006, 61847, 53271, 60192, 62757 };
		uint32 Rogue_Spells[] = { 3127, 674, 48668, 48638, 1784, 48657, 921, 1776, 26669, 51724, 6774, 11305, 1766, 48676, 48659, 1804, 8647, 48691, 51722, 48672, 1725, 26889, 2836, 1833, 1842, 8643, 2094, 1860, 57993, 48674, 31224, 5938, 57934, 51723 };
		uint32 Priest_Spells[] = { 528, 2053, 48161, 48123, 48125, 48066, 586, 48068, 48127, 48171, 48168, 10890, 6064, 988, 48300, 6346, 48071, 48135, 48078, 453, 10955, 10909, 8129, 48073, 605, 48072, 48169, 552, 1706, 48063, 48162, 48170, 48074, 48158, 48120, 34433, 48113, 32375, 64843, 64901, 53023 };
		uint32 Death_Knight_Spells[] = { 50842, 49941, 49930, 47476, 45529, 3714, 56222, 48743, 48263, 49909, 66188, 47528, 45524, 48792, 57623, 56815, 47568, 49895, 50977, 49576, 49921, 46584, 49938, 48707, 48265, 61999, 42650, 53428, 53331, 54447, 53342, 54446, 53323, 53344, 70164, 62158, 33391, 48778, 51425, 49924, 49924 };
		uint32 Shaman_Spells[] = { 2062, 8737, 49273, 49238, 10399, 49231, 58753, 2484, 49281, 58582, 49233, 58790, 58704, 58643, 49277, 61657, 8012, 526, 2645, 57994, 8143, 49236, 58796, 58757, 49276, 57960, 131, 58745, 6196, 58734, 58774, 58739, 58656, 546, 556, 66842, 51994, 8177, 58749, 20608, 36936, 58804, 49271, 8512, 6495, 8170, 66843, 55459, 66844, 3738, 2894, 60043, 51514 };
		uint32 A_Shaman_Spells[] = { 32182 };
		uint32 H_Shaman_Spells[] = { 2825 };
		uint32 Mage_Spells[] = { 7301, 42995, 42833, 27090, 42842, 33717, 42873, 42846, 12826, 28271, 61780, 61721, 28272, 61305, 42917, 43015, 130, 42921, 42926, 43017, 475, 1953, 42940, 12051, 43010, 43020, 43012, 42859, 2139, 42931, 42985, 43008, 45438, 43024, 43002, 43046, 42897, 42914, 66, 58659, 30449, 42956, 47610, 61316, 61024, 55342 };
		uint32 A_Mage_Spells[] = { 32271, 49359, 3565, 33690, 3562, 3561, 11419, 32266, 11416, 33691, 11059, 49360 };
		uint32 H_Mage_Spells[] = { 3567, 35715, 3566, 49358, 32272, 3563, 11417, 35717, 32267, 49361, 11420, 11418 };
		uint32 Warlock_Spells[] = { 1710, 696, 47811, 47809, 688, 47813, 50511, 57946, 47864, 6215, 47878, 47855, 697, 47856, 47857, 5697, 47884, 47815, 47889, 47820, 698, 712, 126, 5138, 5500, 11719, 132, 60220, 18647, 61191, 47823, 691, 47865, 47891, 47888, 17928, 47860, 47825, 1122, 47867, 18540, 47893, 47838, 29858, 58887, 47836, 61290, 48018, 48020, 33388, 33391, 23161 };
		uint32 Druid_Spells[] = { 48378, 48469, 48461, 48463, 48441, 53307, 53308, 5487, 48560, 6795, 48480, 53312, 18960, 5229, 48443, 50763, 8983, 8946, 1066, 48562, 783, 770, 16857, 18658, 768, 1082, 16979, 49376, 5215, 48477, 49800, 48465, 48572, 26995, 48574, 2782, 50213, 2893, 33357, 5209, 48575, 48447, 48577, 48579, 5225, 22842, 49803, 9634, 20719, 48467, 29166, 62600, 22812, 48470, 48564, 48566, 33891, 33943, 49802, 48451, 48568, 33786, 40120, 62078, 52610, 50464, 48570 };

		// Switch For Actions.
		switch (action)
		{
			case 101:
				//Get Class
				switch (player->getClass())
				{
					case CLASS_WARRIOR:
						TeachSpells(player, Warrior_Spells, 42);

						// Talents
						if (player->HasSpell(12294))
							player->LearnSpell(47486, false);

						if (player->HasSpell(20243))
							player->LearnSpell(47498, false);
						break;

					case CLASS_PALADIN:
						TeachSpells(player, Paladin_Spells, 51);
						if (player->GetTeam() == ALLIANCE)
							TeachSpells(player, A_Paladin_Spells, 3);
						else
							TeachSpells(player, H_Paladin_Spells, 3);

						// Talents
						if (player->HasSpell(20925))
							player->LearnSpell(48952, false);

						if (player->HasSpell(31935))
							player->LearnSpell(48827, false);

						if (player->HasSpell(20473))
							player->LearnSpell(48825, false);
						break;

					case CLASS_HUNTER:
						TeachSpells(player, Hunter_Spells, 59);

						// Talents
						if (player->HasSpell(19386))
							player->LearnSpell(49012, false);

						if (player->HasSpell(53301))
							player->LearnSpell(60053, false);

						if (player->HasSpell(19306))
							player->LearnSpell(48999, false);

						if (player->HasSpell(19434))
							player->LearnSpell(49050, false);
						break;

					case CLASS_ROGUE:
						TeachSpells(player, Rogue_Spells, 34);

						// Talents
						if (player->HasSpell(16511))
							player->LearnSpell(48660, false);

						if (player->HasSpell(1329))
							player->LearnSpell(48666, false);

						break;

					case CLASS_PRIEST:
						TeachSpells(player, Priest_Spells, 41);

						// Talents
						if (player->HasSpell(34914))
							player->LearnSpell(48160, false);

						if (player->HasSpell(47540))
							player->LearnSpell(53007, false);

						if (player->HasSpell(724))
							player->LearnSpell(48087, false);

						if (player->HasSpell(19236))
							player->LearnSpell(48173, false);

						if (player->HasSpell(34861))
							player->LearnSpell(48089, false);

						if (player->HasSpell(15407))
							player->LearnSpell(48156, false);
						break;

					case CLASS_DEATH_KNIGHT:
						TeachSpells(player, Death_Knight_Spells, 41);

						// Talents
						if (player->HasSpell(55050))
							player->LearnSpell(55262, false);

						if (player->HasSpell(49143))
							player->LearnSpell(55268, false);

						if (player->HasSpell(49184))
							player->LearnSpell(51411, false);

						if (player->HasSpell(55090))
							player->LearnSpell(55271, false);

						if (player->HasSpell(49158))
							player->LearnSpell(51328, false);
						break;

					case CLASS_SHAMAN:
						TeachSpells(player, Shaman_Spells, 53);
						if (player->GetTeam() == ALLIANCE)
							TeachSpells(player, A_Shaman_Spells, 1);
						else
							TeachSpells(player, H_Shaman_Spells, 1);

						// Talents
						if (player->HasSpell(61295))
							player->LearnSpell(61301, false);

						if (player->HasSpell(974))
							player->LearnSpell(49284, false);

						if (player->HasSpell(30706))
							player->LearnSpell(57722, false);

						if (player->HasSpell(51490))
							player->LearnSpell(59159, false);
						break;

					case CLASS_MAGE:
						TeachSpells(player, Mage_Spells, 46);
						if (player->GetTeam() == ALLIANCE)
							TeachSpells(player, A_Mage_Spells, 12);
						else
							TeachSpells(player, H_Mage_Spells, 12);

						// Talents
						if (player->HasSpell(11366))
							player->LearnSpell(42891, false);

						if (player->HasSpell(11426))
							player->LearnSpell(43039, false);

						if (player->HasSpell(44457))
							player->LearnSpell(55360, false);

						if (player->HasSpell(31661))
							player->LearnSpell(42950, false);

						if (player->HasSpell(11113))
							player->LearnSpell(42945, false);

						if (player->HasSpell(44425))
							player->LearnSpell(44781, false);
						break;

					case CLASS_WARLOCK:
						TeachSpells(player, Warlock_Spells, 52);

						// Talents
						if (player->HasSpell(17877))
							player->LearnSpell(47827, false);

						if (player->HasSpell(30283))
							player->LearnSpell(47847, false);

						if (player->HasSpell(30108))
							player->LearnSpell(47843, false);

						if (player->HasSpell(50796))
							player->LearnSpell(59172, false);

						if (player->HasSpell(48181))
							player->LearnSpell(59164, false);

						if (player->HasSpell(18220))
							player->LearnSpell(59092, false);
						break;

					case CLASS_DRUID:
						TeachSpells(player, Druid_Spells, 67);

						// Talents
						if (player->HasSpell(50516))
							player->LearnSpell(61384, false);

						if (player->HasSpell(48505))
							player->LearnSpell(53201, false);

						if (player->HasSpell(48438))
							player->LearnSpell(53251, false);

						if (player->HasSpell(5570))
							player->LearnSpell(48468, false);
						break;
				}
				// Close Window
				player->PlayerTalkClass->SendCloseGossip();
				// Send Message To Player.
				ChatHandler(player->GetSession()).PSendSysMessage("You have been taught your class spells. If you haven't learned all your talents yet do so and talk to me again because you may have more to learn.");
				break;

			// Glyph Vendor TODO!
			case 102:
				player->PlayerTalkClass->SendCloseGossip();
				break;
		}
		return true;
	}
};

void AddSC_trainer()
{
	new trainer();
}
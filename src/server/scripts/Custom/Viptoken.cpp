#include "ScriptMgr.h"
#include "Item.h"
#include "Player.h"

class vip_token_item1 : public ItemScript
{
public: vip_token_item1() : ItemScript("vip_token_item1") { }
       
	bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/)
	{
		switch (player->GetSession()->GetSecurity())
		{
			case 0:
				LoginDatabase.PQuery("REPLACE INTO `account_access` (`id`, `gmlevel`, `RealmID`, `Active`) VALUES ('%u', '1', '-1', NULL)", player->GetSession()->GetAccountId());
				if (player->HasItemCount(item->GetEntry(), 1, true))
					player->DestroyItemCount(item->GetEntry(), 1, true, false);
				ChatHandler(player->GetSession()).PSendSysMessage("[Strong-WoW] You are now a VIP! Close and ReOpen Wow to take effect!");
				return true;
				break;

			case 1:
				ChatHandler(player->GetSession()).PSendSysMessage("[Strong-WoW] You are already a VIP!");
				return false;
				break;

			default:
				ChatHandler(player->GetSession()).PSendSysMessage("[Strong-WoW] You are a staff member you can not have vip as your security level is higher.");
				return false;
				break;
		}
	}
};
 
void AddSC_vip_token_item1()
{
	new vip_token_item1();
}
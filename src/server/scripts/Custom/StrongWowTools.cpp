#include "Player.h"
#include "Define.h"
#include "Creature.h"
#include "ScriptMgr.h"
#include "WorldSession.h"
#include "ScriptedGossip.h"

enum Token
{
	Customise_Token = 9991010
};

class StrongWow_Tools : public CreatureScript
{
public:
	StrongWow_Tools() : CreatureScript("StrongWow_Tools"){}

	bool OnGossipHello(Player* player, Creature* Creature)
	{
		{
			player->ADD_GOSSIP_ITEM(2, "Player Tools", GOSSIP_SENDER_MAIN, 1);
			player->ADD_GOSSIP_ITEM(2, "Player Buffs", GOSSIP_SENDER_MAIN, 2);
			player->ADD_GOSSIP_ITEM(2, "Player Customize", GOSSIP_SENDER_MAIN, 3);
		}
		player->PlayerTalkClass->SendGossipMenu(111012, Creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction)
	{
		WorldSession* ws = player->GetSession();

		switch (uiAction)
		{
		case 1:  // Charecter Tools
		{
			player->ADD_GOSSIP_ITEM(2, "Heal Me", GOSSIP_SENDER_MAIN, 4);
			player->ADD_GOSSIP_ITEM(2, "Remove Ress Sickness", GOSSIP_SENDER_MAIN, 5);
			player->ADD_GOSSIP_ITEM(2, "Remove Deserter", GOSSIP_SENDER_MAIN, 6);
			player->ADD_GOSSIP_ITEM(2, "Repair Items", GOSSIP_SENDER_MAIN, 7);
			player->ADD_GOSSIP_ITEM(2, "Max Skills", GOSSIP_SENDER_MAIN, 8);
		}
		break;

		case 2: // BUFFS
		{
			player->CastSpell(player, 48469, true); // Mark Wild
			player->CastSpell(player, 58054, true); // Kings
			player->CastSpell(player, 42995, true); // Arcane Intel
			player->CastSpell(player, 48161, true); // Power Word
			player->CastSpell(player, 48169, true); // Shad Pro
			player->CastSpell(player, 48073, true); // Devine Spirit
			player->CastSpell(player, 26393, true); // Elune Blessing
			player->CastSpell(player, 16618, true); // Spirit of the Wind
			player->CastSpell(player, 16612, true); // Agamaggan's Strength
			player->CastSpell(player, 17013, true); // Agamaggan's Agility
			player->CastSpell(player, 7764, true); // Wisdom Of Agamaggan
			player->CastSpell(player, 10767, true); // Rising Spirit
			creature->Whisper("You have been buffed!!  Enjoy your buffs!!", LANG_UNIVERSAL, player);
			player->CLOSE_GOSSIP_MENU();
			return false;
		}
		break;

		case 3: // Player Customise - REQUIRES TOKEN
		{
			player->ADD_GOSSIP_ITEM(2, "Customize Charecter / Name Change", GOSSIP_SENDER_MAIN, 9);
			player->ADD_GOSSIP_ITEM(2, "Race Change", GOSSIP_SENDER_MAIN, 10);
			player->ADD_GOSSIP_ITEM(2, "Faction Change", GOSSIP_SENDER_MAIN, 11);			
		}
		break;

		case 4: // Heal Me

			if (player->IsInCombat())
			{
				creature->Whisper("You are in combat", LANG_UNIVERSAL, player);
			}
			else
		{
			player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
			creature->Whisper("You have been healed!", LANG_UNIVERSAL, player);
			return false;
		}
		break;

		case 5: // Remove Ress Sick
			if (player->HasAura(26013))
			{
				player->RemoveAura(26013);
				creature->Whisper("Ressurection Sickness has been removed", LANG_UNIVERSAL, player);
				return false;
			}
			break;

		case 6: // Remove Deserter
			if (player->HasAura(15007))
			{
				player->RemoveAura(15007);
				creature->Whisper("Deserter has been removed", LANG_UNIVERSAL, player);
			}
			else
			{
				creature->Whisper("You are currently not a deserter", LANG_UNIVERSAL, player);
				return false;
			}
			break;

		case 7: // Repair Items
		{
			player->DurabilityRepairAll(false, 0.0f, true);
			creature->Whisper("I repaired all your items, including items from bank.", LANG_UNIVERSAL, player);
			return false;
		}
		break;

		case 8: // Max Skills
		{
			player->UpdateSkillsToMaxSkillsForLevel();
			creature->Whisper("Your skills have been advanced to maximum level.", LANG_UNIVERSAL, player);
			return false;
		}
		break;

		case 9: // Name Change - Customise
			if (player->HasItemCount(Customise_Token, 1))
			{
				player->DestroyItemCount(Customise_Token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
				player->GetSession()->SendNotification("You need to relog, to customize!");
			}
			else
			{
				player->GetSession()->SendNotification("You require a change token!");
				return false;
			}
			break;

		case 10: // Race Change
			if (player->HasItemCount(Customise_Token, 1))
			{
				player->DestroyItemCount(Customise_Token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
				player->GetSession()->SendNotification("You need to relog, to change your race!");
			}
			else
			{
				player->GetSession()->SendNotification("You require a change token!");
				return false;
			}
			break;

		case 11: // Faction Change
			if (player->HasItemCount(Customise_Token, 1))
			{
				player->DestroyItemCount(Customise_Token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
				player->GetSession()->SendNotification("You need to relog, to change your faction!");
			}
			else
			{
				player->GetSession()->SendNotification("You require a change token!");
				return false;
			}
			break;

		}
		return true;
		}
	};


	void AddSC_StrongWow_Tools()
	{
		new StrongWow_Tools();
	}
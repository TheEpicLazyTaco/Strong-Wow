#include "Chat.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"

/* SQL QUERYS
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip status', '1218', 'vip status');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip upgrade', '1217', 'vip upgrade');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip appear', '1216', 'vip appear');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip summon', '1215', 'vip summon');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip mail', '1214', 'vip mail');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip bank', '1213', 'vip bank');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip scale', '1212', 'vip scale');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip demorph', '1211', 'vip demorph');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip morph', '1210', 'vip morph');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip unbuff', '1209', 'vip unbuff');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip Suicide', '1208', 'vip Suicide');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip key', '1207', 'vip key');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip buff', '1206', 'vip buff');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip respawn', '1205', 'vip respawn');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip customize', '1204', 'vip customize');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip faction', '1203', 'vip faction');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip race', '1202', 'vip race');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip instance', '1201', 'vip instance');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('vip', '1200', 'vip');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('stuck', '1118', 'stuck');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('mall', '1117', 'mall');
REPLACE INTO `command` (`name`, `permission`, `help`) VALUES ('maxskills', '1116', 'maxskills');

REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1218', 'VIP status');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1217', 'VIP upgrade');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1216', 'VIP appear');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1215', 'VIP summon');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1214', 'VIP mail');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1213', 'VIP bank');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1212', 'VIP scale');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1211', 'VIP demorph');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1210', 'VIP morph');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1209', 'VIP unbuff');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1208', 'VIP Suicide');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1207', 'VIP key');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1206', 'VIP buff');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1205', 'VIP respawn');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1204', 'VIP customize');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1203', 'VIP faction');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1202', 'VIP race');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1201', 'VIP instance');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1200', 'VIP');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1118', 'stuck');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1117', 'mall');
REPLACE INTO `rbac_permissions` (`id`, `name`) VALUES ('1116', 'maxskills');

REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1218');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1217');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1216');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1215');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1214');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1213');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1212');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1211');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1210');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1209');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1208');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1207');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1206');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1205');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1204');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1203');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1202');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1201');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1200');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1118');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1117');
REPLACE INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES ('192', '1116');

Also make sure you add a column in accounts named vipRank.
*/

class vip_commandscript : public CommandScript
{
public:
	vip_commandscript() : CommandScript("vip_commandscript") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> vipCommandTable =
		{
			{ "instance", rbac::RBAC_PERM_VIP_INSTANCE, false, &HandleVipInstanceCommand, "" },
			{ "race", rbac::RBAC_PERM_VIP_CHANGERACE, false, &HandleChangeRaceCommand, "" },
			{ "faction", rbac::RBAC_PERM_VIP_CHANGEFACTION, false, &HandleChangeFactionCommand, "" },
			{ "customize", rbac::RBAC_PERM_VIP_CUSTOMIZE, false, &HandleCustomizeCommand, "" },
			{ "respawn", rbac::RBAC_PERM_VIP_RESPAWN, false, &HandleVipRespawnCommand, "" },
			{ "buff", rbac::RBAC_PERM_VIP_BUFF, false, &HandleVipBuffCommand, "" },
			{ "key", rbac::RBAC_PERM_VIP_KEY, false, &HandleVipKeyCommand, "" },
			{ "suicide", rbac::RBAC_PERM_VIP_SUICIDE, false, &HandleVipSuicideCommand, "" },
			{ "unbuff", rbac::RBAC_PERM_VIP_UNBUFF, false, &HandleVipUnbuffCommand, "" },
			{ "morph", rbac::RBAC_PERM_VIP_MORPH, false, &HandleVipMorphCommand, "" },
			{ "demorph", rbac::RBAC_PERM_VIP_DEMORPH, false, &HandleVipDeMorphCommand, "" },
			{ "scale", rbac::RBAC_PERM_VIP_SCALE, false, &HandleVipScaleCommand, "" },
			{ "bank", rbac::RBAC_PERM_VIP_BANK, false, &HandleVipBankCommand, "" },
			{ "mail", rbac::RBAC_PERM_VIP_MAIL, false, &HandleVipMailCommand, "" },
			{ "summon", rbac::RBAC_PERM_VIP_SUMMON, false, &HandleVipSummonCommand, "" },
			{ "appear", rbac::RBAC_PERM_VIP_APPEAR, false, &HandleVipAppearCommand, "" },
			{ "upgrade", rbac::RBAC_PERM_VIP_UPGRADE, false, &HandleVipUpgradeCommand, "" },
			{ "status", rbac::RBAC_PERM_VIP_STATUS, false, &HandleVipStatusCommand, "" },
			{ "", rbac::RBAC_PERM_VIP, false, &HandleVipCommand, "" },
		};
		static std::vector<ChatCommand> commandTable =
		{
			{ "vip", rbac::RBAC_PERM_COMMAND_WP, false, NULL, "", vipCommandTable },
		};
		return commandTable;
	}

	static bool HandleVipCommand(ChatHandler* handler, const char* /*args*/)
	{
		handler->PSendSysMessage("Command List:");
		return true;
	}

	static bool HandleChangeRaceCommand(ChatHandler* handler, const char* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		handler->PSendSysMessage("Relog to change race of your character.   --Strong-WoW--");
		return true;
	}

	static bool HandleChangeFactionCommand(ChatHandler* handler, const char* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		handler->PSendSysMessage("Relog to change faction of your character.   --Strong-WoW--");
		return true;
	}

	static bool HandleCustomizeCommand(ChatHandler* handler, const char* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		handler->PSendSysMessage("Relog to customize your character.   --Strong-WoW--");
		return true;
	}

	static bool HandleVipInstanceCommand(ChatHandler* handler, const char* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->IsInCombat())
		{
			handler->SendSysMessage(LANG_YOU_IN_COMBAT);
			handler->SetSentErrorMessage(true);
			return false;
		}

		// stop flight if need
		if (player->IsInFlight())
		{
			player->GetMotionMaster()->MovementExpired();
			player->CleanupAfterTaxiFlight();
		}
		// stop flight if need
		if (player->IsInFlight())
		{
			player->GetMotionMaster()->MovementExpired();
			player->CleanupAfterTaxiFlight();
		}
		// save only in non-flight case
		else
			player->SaveRecallPosition();

		player->TeleportTo(585, 7.09f, -0.45f, -2.8f, 6.22973f); // MapId, X, Y, Z, O
		handler->PSendSysMessage("You Have Been Teleported!    --Strong-WoW--");
		return true;
	}

	static bool HandleVipRespawnCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		Creature* target = player->GetTarget() ? handler->getSelectedCreature() : nullptr;
		if (target)
		{
			if (target->IsPet())
			{
				handler->SendSysMessage(LANG_SELECT_CREATURE);
				handler->SetSentErrorMessage(true);
				return false;
			}

			if (target->isDead())
			{
				target->Respawn();
				handler->PSendSysMessage("Creature Respawned.");
			}
			return true;
		}
		return false;
	}

	static bool HandleVipBuffCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		uint32 auras[] = { 16612, 26035, 31305, 36001, 70242, 70244, 30089, 70243 };

		for (int i = 0; i < 8; i++)
			player->AddAura(auras[i], player);
		handler->PSendSysMessage("You have been buffed, enjoy!");
		return true;
	}

	static bool HandleVipKeyCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		uint32 vipkey = 438555;

		if (!player->HasItemCount(vipkey, 1, false)) {
			player->AddItem(vipkey, 1);
			handler->PSendSysMessage("You have been given the vip key!");
		}
		else {
			handler->PSendSysMessage("You already have the key!");
		}
		return true;
	}

	static bool HandleVipSuicideCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->IsAlive())
		{
			player->Kill(player);
			handler->PSendSysMessage("You have commited suicide.");
		}
		return true;
	}

	static bool HandleVipUnbuffCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->RemoveAllAuras();
		handler->PSendSysMessage("You have removed all your buffs.");
		return true;
	}

	static bool HandleVipMorphCommand(ChatHandler* handler, char const* args)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (!*args)
			return false;

		uint16 display_id = (uint16)atoi((char*)args);

		player->SetDisplayId(display_id);
		
		handler->PSendSysMessage("You have morphed yourself.");
		return true;
	}

	static bool HandleVipDeMorphCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();
		player->DeMorph();
		handler->PSendSysMessage("You have been demorphed.");
		return true;
	}

	static bool HandleVipScaleCommand(ChatHandler* handler, char const* args)
	{
		if (!*args)
			return false;

		Player* player = handler->GetSession()->GetPlayer();
		float Scale = (float)atof((char*)args);

		if (Scale > 1.5f || Scale < 0.5f)
		{
			handler->SendSysMessage("Scale value must be between 0.5 and 1.5!");
			handler->SetSentErrorMessage(true);
			return false;
		}

		player->SetObjectScale(Scale);
		handler->PSendSysMessage("You have scaled yourself.");
		return true;
	}


	static bool HandleVipBankCommand(ChatHandler* handler, char const* /*args*/)
	{
		handler->GetSession()->SendShowBank(handler->GetSession()->GetPlayer()->GetGUID());
		return true;
	}

	static bool HandleVipMailCommand(ChatHandler* handler, char const* /*args*/)
	{
		handler->GetSession()->SendShowMailBox(handler->GetSession()->GetPlayer()->GetGUID());
		return true;
	}

	static bool HandleVipSummonCommand(ChatHandler* handler, char const* args)
	{
		Player* target;
		ObjectGuid targetGuid;
		std::string targetName;

		if (!handler->extractPlayerTarget((char*)args, &target, &targetGuid, &targetName))
		{
			handler->PSendSysMessage("Could not find that player.");
			return false;
		}

		Player* _player = handler->GetSession()->GetPlayer();
		
		target->SendSummonRequestFrom(_player);
		return true;
	}

	static bool HandleVipAppearCommand(ChatHandler* handler, char const* args)
	{
		handler->PSendSysMessage("Appear Command");
		return true;
	}

	static bool HandleVipUpgradeCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		uint32 upgradeItem_1 = 1;
		uint32 upgradeItem_2 = 2;
		uint32 upgradeItem_3 = 3;

		switch (player->vipRank)
		{
		case 0:
			if (player->HasItemCount(upgradeItem_1, 1, false))
			{
				player->SetVipRank(player);
				player->DestroyItemCount(upgradeItem_1, 1, true, false);
				handler->PSendSysMessage("You are now Rank 1!");
			}
			else
				handler->PSendSysMessage("You do not have the Rank 1 Upgrade Token!");
			break;
			break;
		case 1:
			if (player->HasItemCount(upgradeItem_2, 1, false))
			{
				player->SetVipRank(player);
				player->DestroyItemCount(upgradeItem_2, 1, true, false);
				handler->PSendSysMessage("You are now Rank 2!");
			}
			else
				handler->PSendSysMessage("You do not have the Rank 2 Upgrade Token!");
			break;
			break;
		case 2:
			if (player->HasItemCount(upgradeItem_3, 1, false))
			{
				player->SetVipRank(player);
				player->DestroyItemCount(upgradeItem_3, 1, true, false);
				handler->PSendSysMessage("You are now Rank 3!");
			} 
			else
				handler->PSendSysMessage("You do not have the Rank 3 Upgrade Token!");
			break;
		case 3:
			handler->PSendSysMessage("You are already rank 3 and cant not upgrade any more.");
			break;
		}
		return true;
	}

	static bool HandleVipStatusCommand(ChatHandler* handler, char const* /*args*/)
	{
		//Needs to return vip rank name.
		return true;
	}

};

void AddSC_vip_commandscript()
{
	new vip_commandscript();
}
#include "Chat.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"

class player_commandscript : public CommandScript
{
public:
	player_commandscript() : CommandScript("player_commandscript") { }

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> commandTable =
		{
			{ "maxskills", rbac::RBAC_PERM_MAXSKILLS, false, &HandleMaxSkillsCommand, "" },
			{ "mall", rbac::RBAC_PERM_MALL, false, &HandleVipMallCommand, "" },
			{ "stuck", rbac::RBAC_PERM_UNSTUCK, false, &HandleVipUnstuckCommand, "" },
		};
		return commandTable;
	}
	
	static bool HandleMaxSkillsCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->UpdateSkillsToMaxSkillsForLevel();
		handler->PSendSysMessage("Your weapon skills are now maximized.   --Strong-WoW--");
		return true;
	}

	static bool HandleVipMallCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		player->TeleportTo(1, 132.04f, 1363.79, 199.08f, 5.55f);
		handler->PSendSysMessage("You have been teleported to the mall.");
		return true;
	}

	static bool HandleVipUnstuckCommand(ChatHandler* handler, char const* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->isDead())
		{
			player->ResurrectPlayer(1.0f, false);
			player->SpawnCorpseBones();
			player->SaveToDB();
		}

		player->TeleportTo(1, 132.04f, 1363.79, 199.08f, 5.55f);
		handler->PSendSysMessage("If you were dead you were revived and you were also teleported to the mall.");
		return true;
	}
};

void AddSC_player_commandscript()
{
	new player_commandscript();
}